﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Kernel.ByteRepresentation
{
    public ref struct ByteDeserializer
    {
        private readonly ReadOnlySpan<byte> _buffer;
        private int _offset;

        public ByteDeserializer(ReadOnlySpan<byte> buffer)
        {
            _buffer = buffer;
            _offset = 0;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public byte RByte() => _buffer[_offset++];

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public sbyte RSbyte() => (sbyte) _buffer[_offset++];

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public bool RBool() => RByte() == 1;

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe short RShort()
        {
            short result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(short))) result = *(short*)ptr;
            _offset += sizeof(short);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe ushort RuShort()
        {
            ushort result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(ushort))) result = *(ushort*)ptr;
            _offset += sizeof(ushort);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe char RChar()
        {
            char result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(char))) result = *(char*)ptr;
            _offset += sizeof(char);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe int RInt()
        {
            int result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(int))) result = *(int*)ptr;
            _offset += sizeof(int);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe uint RuInt()
        {
            uint result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(uint))) result = *(uint*)ptr;
            _offset += sizeof(uint);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe float RFloat()
        {
            float result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(float))) result = *(float*)ptr;
            _offset += sizeof(float);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe long RLong()
        {
            long result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(long))) result = *(long*)ptr;
            _offset += sizeof(long);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe ulong RuLong()
        {
            ulong result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(ulong))) result = *(ulong*)ptr;
            _offset += sizeof(ulong);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe double RDouble()
        {
            double result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(double))) result = *(double*)ptr;
            _offset += sizeof(double);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe DateTime RDateTime()
        {
            DateTime result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(DateTime))) result = *(DateTime*)ptr;
            _offset += sizeof(DateTime);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe DateTimeOffset RDateTimeOffset()
        {
            DateTimeOffset result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(DateTimeOffset))) result = *(DateTimeOffset*)ptr;
            _offset += sizeof(DateTimeOffset);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe TimeSpan RTimeSpan()
        {
            TimeSpan result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(TimeSpan))) result = *(TimeSpan*)ptr;
            _offset += sizeof(TimeSpan);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe Guid RGuid()
        {
            Guid result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(Guid))) result = *(Guid*)ptr;
            _offset += sizeof(Guid);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe decimal RDecimal()
        {
            decimal result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(decimal))) result = *(decimal*)ptr;
            _offset += sizeof(decimal);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public byte? RByteNullable() => _buffer[_offset++] == 1 ? _buffer[_offset++] : (byte?)null;

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public sbyte? RsByteNullable() => _buffer[_offset++] == 1 ? (sbyte?)_buffer[_offset++] : null;

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public bool? RBoolNullable() => _buffer[_offset++] == 1 ? _buffer[_offset++] == 1 : (bool?) null;

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe short? RShortNullable()
        {
            if (_buffer[_offset++] != 1) return null;
            short result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(short))) result = *(short*) ptr;
            _offset += sizeof(short);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe ushort? RuShortNullable()
        {
            if (_buffer[_offset++] != 1) return null;
            ushort result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(ushort))) result = *(ushort*)ptr;
            _offset += sizeof(ushort);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe char? RCharNullable()
        {
            if (_buffer[_offset++] != 1) return null;
            char result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(char))) result = *(char*)ptr;
            _offset += sizeof(char);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe int? RIntNullable()
        {
            if (_buffer[_offset++] != 1) return null;
            int result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(int))) result = *(int*)ptr;
            _offset += sizeof(int);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe uint? RuIntNullable()
        {
            if (_buffer[_offset++] != 1) return null;
            uint result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(uint))) result = *(uint*)ptr;
            _offset += sizeof(uint);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe float? RFloatNullable()
        {
            if (_buffer[_offset++] != 1) return null;
            float result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(float))) result = *(float*)ptr;
            _offset += sizeof(float);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe long? RLongNullable()
        {
            if (_buffer[_offset++] != 1) return null;
            long result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(long))) result = *(long*)ptr;
            _offset += sizeof(long);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe ulong? RuLongNullable()
        {
            if (_buffer[_offset++] != 1) return null;
            ulong result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(ulong))) result = *(ulong*)ptr;
            _offset += sizeof(ulong);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe double? RDoubleNullable()
        {
            if (_buffer[_offset++] != 1) return null;
            double result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(double))) result = *(double*)ptr;
            _offset += sizeof(double);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe DateTime? RDateTimeNullable()
        {
            if (_buffer[_offset++] != 1) return null;
            DateTime result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(DateTime))) result = *(DateTime*)ptr;
            _offset += sizeof(DateTime);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe DateTimeOffset? RDateTimeOffsetNullable()
        {
            if (_buffer[_offset++] != 1) return null;
            DateTimeOffset result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(DateTimeOffset))) result = *(DateTimeOffset*)ptr;
            _offset += sizeof(DateTimeOffset);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe TimeSpan? RTimeSpanNullable()
        {
            if (_buffer[_offset++] != 1) return null;
            TimeSpan result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(TimeSpan))) result = *(TimeSpan*)ptr;
            _offset += sizeof(TimeSpan);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe Guid? RGuidNullable()
        {
            if (_buffer[_offset++] != 1) return null;
            Guid result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(Guid))) result = *(Guid*)ptr;
            _offset += sizeof(Guid);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe decimal? RDecimalNullable()
        {
            if (_buffer[_offset++] != 1) return null;
            decimal result;
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(decimal))) result = *(decimal*)ptr;
            _offset += sizeof(decimal);
            return result;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public string RString(Encoding encoding)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> RList<T>()
        {
            throw new NotImplementedException();
        }
    }
}
