﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Kernel.ByteRepresentation
{
    public ref struct ByteSerializer
    {
        private readonly Span<byte> _buffer;
        private int _offset;

        public ByteSerializer(BufferSizeMode serializerSizeMode)
        {
            _buffer = serializerSizeMode switch
            {
                BufferSizeMode.Small => new byte[sizeof(byte)],
                BufferSizeMode.Mid => new byte[sizeof(short)],
                BufferSizeMode.Large => new byte[12 * 1024 * 1024],
                _ => throw new ArgumentOutOfRangeException(nameof(serializerSizeMode), serializerSizeMode, null)
            };
            _offset = 0;
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Write(byte @byte) => _buffer[_offset++] = @byte;

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Write(sbyte @sbyte) => _buffer[_offset++] = (byte) @sbyte;

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Write(bool @bool) => Write((byte)(@bool ? 1 : 0));

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(short @short)
        {
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(short))) *(short*)ptr = @short;
            _offset += sizeof(short);
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(ushort @ushort)
        {
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(ushort))) *(ushort*)ptr = @ushort;
            _offset += sizeof(ushort);
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(char @char)
        {
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(char))) *(char*)ptr = @char;
            _offset += sizeof(ushort);
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(int @int)
        {
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(int))) *(int*)ptr = @int;
            _offset += sizeof(int);
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(uint @uint)
        {
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(uint))) *(uint*)ptr = @uint;
            _offset += sizeof(uint);
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(float @float)
        {
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(float))) *(float*)ptr = @float;
            _offset += sizeof(float);
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(long @long)
        {
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(long))) *(long*)ptr = @long;
            _offset += sizeof(long);
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(ulong @ulong)
        {
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(ulong))) *(ulong*)ptr = @ulong;
            _offset += sizeof(ulong);
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(double @double)
        {
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(double))) *(double*)ptr = @double;
            _offset += sizeof(double);
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(DateTime dateTime)
        {
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(DateTime))) *(DateTime*)ptr = dateTime;
            _offset += sizeof(DateTime);
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(DateTimeOffset dateTimeOffset)
        {
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(DateTimeOffset))) *(DateTimeOffset*)ptr = dateTimeOffset;
            _offset += sizeof(DateTimeOffset);
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(TimeSpan timeSpan)
        {
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(TimeSpan))) *(TimeSpan*)ptr = timeSpan;
            _offset += sizeof(TimeSpan);
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(Guid guid)
        {
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(Guid))) *(Guid*)ptr = guid;
            _offset += sizeof(Guid);
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(decimal @decimal)
        {
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(decimal))) *(decimal*)ptr = @decimal;
            _offset += sizeof(decimal);
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Write(byte? @byte)
        {
            if (@byte is null) _buffer[_offset++] = 0;
            else
            {
                _buffer[_offset++] = 1;
                _buffer[_offset++] = @byte.Value;
            }
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Write(sbyte? @sbyte)
        {
            if (@sbyte is null) _buffer[_offset++] = 0;
            else
            {
                _buffer[_offset++] = 1;
                _buffer[_offset++] = (byte) @sbyte.Value;
            }
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Write(bool? @bool)
        {
            if (@bool is null) _buffer[_offset++] = 0;
            else
            {
                _buffer[_offset++] = 1;
                _buffer[_offset++] = (byte)(@bool.Value ? 1 : 0);
            }
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(short? @short)
        {
            if (@short is null) _buffer[_offset++] = 0;
            else
            {
                _buffer[_offset++] = 1;
                fixed (byte* ptr = _buffer.Slice(_offset, sizeof(short))) *(short*)ptr = @short.Value;
                _offset += sizeof(short);
            }
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(ushort? @ushort)
        {
            if (@ushort is null) _buffer[_offset++] = 0;
            else
            {
                _buffer[_offset++] = 1;
                fixed (byte* ptr = _buffer.Slice(_offset, sizeof(ushort))) *(ushort*)ptr = @ushort.Value;
                _offset += sizeof(ushort);
            }
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(char? @char)
        {
            if (@char is null) _buffer[_offset++] = 0;
            else
            {
                _buffer[_offset++] = 1;
                fixed (byte* ptr = _buffer.Slice(_offset, sizeof(char))) *(char*)ptr = @char.Value;
                _offset += sizeof(char);
            }
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(int? @int)
        {
            if (@int is null) _buffer[_offset++] = 0;
            else
            {
                _buffer[_offset++] = 1;
                fixed (byte* ptr = _buffer.Slice(_offset, sizeof(int))) *(int*)ptr = @int.Value;
                _offset += sizeof(int);
            }
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(uint? @uint)
        {
            if (@uint is null) _buffer[_offset++] = 0;
            else
            {
                _buffer[_offset++] = 1;
                fixed (byte* ptr = _buffer.Slice(_offset, sizeof(uint))) *(uint*)ptr = @uint.Value;
                _offset += sizeof(uint);
            }
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(float? @float)
        {
            if (@float is null) _buffer[_offset++] = 0;
            else
            {
                _buffer[_offset++] = 1;
                fixed (byte* ptr = _buffer.Slice(_offset, sizeof(float))) *(float*)ptr = @float.Value;
                _offset += sizeof(float);
            }
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(long? @long)
        {
            if (@long is null) _buffer[_offset++] = 0;
            else
            {
                _buffer[_offset++] = 1;
                fixed (byte* ptr = _buffer.Slice(_offset, sizeof(long))) *(long*)ptr = @long.Value;
                _offset += sizeof(long);
            }
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(ulong? @ulong)
        {
            if (@ulong is null) _buffer[_offset++] = 0;
            else
            {
                _buffer[_offset++] = 1;
                fixed (byte* ptr = _buffer.Slice(_offset, sizeof(ulong))) *(ulong*)ptr = @ulong.Value;
                _offset += sizeof(ulong);
            }
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(double? @double)
        {
            if (@double is null) _buffer[_offset++] = 0;
            else
            {
                _buffer[_offset++] = 1;
                fixed (byte* ptr = _buffer.Slice(_offset, sizeof(double))) *(double*)ptr = @double.Value;
                _offset += sizeof(double);
            }
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(DateTime? dateTime)
        {
            if (dateTime is null) _buffer[_offset++] = 0;
            else
            {
                _buffer[_offset++] = 1;
                fixed (byte* ptr = _buffer.Slice(_offset, sizeof(DateTime))) *(DateTime*)ptr = dateTime.Value;
                _offset += sizeof(DateTime);
            }
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(DateTimeOffset? dateTimeOffset)
        {
            if (dateTimeOffset is null) _buffer[_offset++] = 0;
            else
            {
                _buffer[_offset++] = 1;
                fixed (byte* ptr = _buffer.Slice(_offset, sizeof(DateTimeOffset))) *(DateTimeOffset*)ptr = dateTimeOffset.Value;
                _offset += sizeof(DateTimeOffset);
            }
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(TimeSpan? timeSpan)
        {
            if (timeSpan is null) _buffer[_offset++] = 0;
            else
            {
                _buffer[_offset++] = 1;
                fixed (byte* ptr = _buffer.Slice(_offset, sizeof(TimeSpan))) *(TimeSpan*)ptr = timeSpan.Value;
                _offset += sizeof(TimeSpan);
            }
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(Guid? guid)
        {
            if (guid is null) _buffer[_offset++] = 0;
            else
            {
                _buffer[_offset++] = 1;
                fixed (byte* ptr = _buffer.Slice(_offset, sizeof(Guid))) *(Guid*)ptr = guid.Value;
                _offset += sizeof(Guid);
            }
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(decimal? @decimal)
        {
            if (@decimal is null) _buffer[_offset++] = 0;
            else
            {
                _buffer[_offset++] = 1;
                fixed (byte* ptr = _buffer.Slice(_offset, sizeof(decimal))) *(decimal*)ptr = @decimal.Value;
                _offset += sizeof(decimal);
            }
        }

        public void Write(string @string, Encoding encoding)
        {
            throw new NotImplementedException();
        }

        public void Write<T>(IEnumerable<T> enumerable)
        {
            throw new NotImplementedException();
        }
    }
}
