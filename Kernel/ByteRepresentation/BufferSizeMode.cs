﻿namespace Kernel.ByteRepresentation
{
    /// <summary>
    /// Typed size of ByteSerializer
    /// </summary>
    public enum BufferSizeMode : byte
    {

        /// <summary>
        /// 1 byte of size = 256 bytes
        /// </summary>
        Small = 1,

        /// <summary>
        /// 2 byte of size = 65535 bytes
        /// </summary>
        Mid,

        /// <summary>
        /// 3 bytes of size = 12582912 bytes (12 mb).
        /// <para>For 3 bytes max size = 16777216 bytes, but we must give some free space for other stuff on stack.</para>
        /// <para> To use this you must increase size of thread stack.</para>
        /// <para> Example: "{ProgrammFiles}\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.14.26428\bin\Hostx64\x64\EDITBIN.EXE" /stack:16777216 ExampleProgram.exe</para>
        /// </summary>
        Large
    }
}
