﻿using System;
using System.Runtime.InteropServices;

namespace Kernel.PInvoke
{
    public static class Memory
    {
        [DllImport("kernel32.dll")]
        private static extern int VirtualQuery(IntPtr lpAddress, ref MemoryBasicInformation lpBuffer, int dwLength);
    }
}
