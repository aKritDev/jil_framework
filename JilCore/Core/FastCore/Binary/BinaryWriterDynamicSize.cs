﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using System.Text;
using JilCore.Core.Utils.Contracts;

namespace JilCore.Core.FastCore.Binary
{
    /// <summary>
    /// 
    /// </summary>
    public ref struct BinaryWriterDynamicSize
    {

        #region public (Open class data/logic API ▼▼▼)

        #region data

        /// <summary>
        /// 
        /// </summary>
        public byte[] Buffer => _buffer.Slice(0, _offset).ToArray();

        #endregion

        #region actions

        /// <summary>
        /// Write 1 data byte
        /// </summary>
        /// <param name="byte">data byte</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Write(byte @byte)
        {
            EnsureCapacity(sizeof(byte));
            _buffer[_offset++] = @byte;
        }

        /// <summary>
        /// Write 1 byte(nullable flag) and 1 byte(@byte) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @byte not null {</para>
        /// <para>1 byte: data byte</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="byte">data byte</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Write(byte? @byte)
        {
            if (@byte is null) Write(false);
            else
            {
                Write(true);
                Write((byte)@byte);
            }
        }

        /// <summary>
        /// Write 1 data (byte)sbyte
        /// <para>Signature</para>
        /// <para>1 byte: data (byte)sbyte </para>
        /// </summary>
        /// <param name="sbyte">data sbyte</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Write(sbyte @sbyte) => Write((byte) @sbyte);

        /// <summary>
        /// Write 1 byte(nullable flag) and 1 byte(@sbyte) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @sbyte not null {</para>
        /// <para>1 byte: data (byte)@sbyte</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="sbyte">data sbyte</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Write(sbyte? @sbyte)
        {
            if (@sbyte is null) Write(false);
            else
            {
                Write(true);
                Write((sbyte)@sbyte);
            }
        }

        /// <summary>
        /// Write 1 byte: data (byte)bool
        /// <para>Signature</para>
        /// <para>1 byte: data (byte)bool </para>
        /// </summary>
        /// <param name="bool">data bool</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Write(bool @bool) => Write((byte)(@bool ? 1 : 0));

        /// <summary>
        /// Write 1 byte(nullable flag) and 1 byte(@bool) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @bool not null {</para>
        /// <para>1 byte: data (byte)@bool</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="bool">data bool?</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Write(bool? @bool)
        {
            if (@bool is null) Write(false);
            else
            {
                Write(true);
                Write((bool) @bool);
            }
        }

        /// <summary>
        /// Write two bytes of short
        /// <para>Signature</para>
        /// <para>2 bytes: data short </para>
        /// </summary>
        /// <param name="short">data short</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(short @short)
        {
            EnsureCapacity(sizeof(short));
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(short))) *(short*)ptr = @short;
            _offset += sizeof(short);
        }

        /// <summary>
        /// Write 1 byte(nullable flag) and 2 byte(@short) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @short not null {</para>
        /// <para>2 bytes: data short</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="short">data short?</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Write(short? @short)
        {
            if(@short is null) Write(false);
            else
            {
                Write(true);
                Write((short)@short);
            }
        }

        /// <summary>
        /// Write 2 bytes of ushort
        /// <para>Signature</para>
        /// <para>2 bytes: data ushort </para>
        /// </summary>
        /// <param name="ushort">data short</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Write(ushort @ushort) => Write((short)@ushort);

        /// <summary>
        /// Write 1 byte(nullable flag) and 2 byte(@ushort) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @ushort not null {</para>
        /// <para>2 bytes: data ushort</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="ushort">data ushort?</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Write(ushort? @ushort)
        {
            if (@ushort is null) Write(false);
            else
            {
                Write(true);
                Write((short)@ushort);
            }
        }

        /// <summary>
        /// Write two bytes of char
        /// <para>Signature</para>
        /// <para>2 bytes: data char </para>
        /// </summary>
        /// <param name="char">data short</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(char @char)
        {
            EnsureCapacity(sizeof(char));
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(char))) *(char*)ptr = @char;
            _offset += sizeof(char);
        }

        /// <summary>
        /// Write 1 byte(nullable flag) and 2 byte(@char) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @char not null {</para>
        /// <para>2 bytes: data char</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="char">data char?</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Write(char? @char)
        {
            if (@char is null) Write(false);
            else
            {
                Write(true);
                Write((char)@char);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="int24"></param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void WriteUInt24(uint int24)
        {
            EnsureCapacity(3);
            fixed (byte* ptr = _buffer.Slice(_offset, 3)) *(uint*)ptr = int24;
            _offset += 3;
        }

        /// <summary>
        /// Write 4 bytes of int
        /// <para>Signature</para>
        /// <para>4 bytes: data int </para>
        /// </summary>
        /// <param name="int">data int</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(int @int)
        {
            EnsureCapacity(sizeof(int));
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(int))) *(int*)ptr = @int;
            _offset += sizeof(int);
        }

        /// <summary>
        /// Write 1 byte(nullable flag) and 4 byte(@int) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @int not null {</para>
        /// <para>4 bytes: data int</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="int">data int?</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Write(int? @int)
        {
            if (@int is null) Write(false);
            else
            {
                Write(true);
                Write((int)@int);
            }
        }

        /// <summary>
        /// Write 4 bytes of uint
        /// <para>Signature</para>
        /// <para>4 bytes: data uint</para>
        /// </summary>
        /// <param name="uint">data uint</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Write(uint @uint) => Write((int) @uint);

        /// <summary>
        /// Write 1 byte(nullable flag) and 4 byte(@uint) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @uint not null {</para>
        /// <para>4 bytes: data uint</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="uint">data uint?</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Write(uint? @uint)
        {
            if (@uint is null) Write(false);
            else
            {
                Write(true);
                Write((int)@uint);
            }
        }

        /// <summary>
        /// Write 4 bytes of float
        /// <para>Signature</para>
        /// <para>4 bytes: data float</para>
        /// </summary>
        /// <param name="float">data float</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(float @float)
        {
            EnsureCapacity(sizeof(float));
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(float))) *(float*)ptr = @float;
            _offset += sizeof(float);
        }

        /// <summary>
        /// Write 1 byte(nullable flag) and 4 byte(@float) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @float not null {</para>
        /// <para>4 bytes: data float</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="float">data float?</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Write(float? @float)
        {
            if (@float is null) Write(false);
            else
            {
                Write(true);
                Write((float)@float);
            }
        }

        /// <summary>
        /// Write 8 bytes of long
        /// <para>Signature</para>
        /// <para>8 bytes: data long</para>
        /// </summary>
        /// <param name="long">data long</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(long @long)
        {
            EnsureCapacity(sizeof(long));
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(long))) *(long*)ptr = @long;
            _offset += sizeof(long);
        }

        /// <summary>
        /// Write 1 byte(nullable flag) and 8 byte(@long) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @long not null {</para>
        /// <para>8 bytes: data long</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="long">data long?</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Write(long? @long)
        {
            if (@long is null) Write(false);
            else
            {
                Write(true);
                Write((long)@long);
            }
        }

        /// <summary>
        /// Write 8 bytes of ulong
        /// <para>Signature</para>
        /// <para>8 bytes: data ulong</para>
        /// </summary>
        /// <param name="ulong">data ulong</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Write(ulong @ulong) => Write((long) @ulong);

        /// <summary>
        /// Write 1 byte(nullable flag) and 8 byte(@ulong) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @ulong not null {</para>
        /// <para>8 bytes: data ulong</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="ulong">data ulong?</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Write(ulong? @ulong) 
        {
            if (@ulong is null) Write(false);
            else
            {
                Write(true);
                Write((long)@ulong);
            }
        }

        /// <summary>
        /// Write 8 bytes of double
        /// <para>Signature</para>
        /// <para>8 bytes: data double</para>
        /// </summary>
        /// <param name="double">data double</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(double @double)
        {
            EnsureCapacity(sizeof(double));
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(double))) *(double*)ptr = @double;
            _offset +=  sizeof(double);
        }

        /// <summary>
        /// Write 1 byte(nullable flag) and 8 byte(@double) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @double not null {</para>
        /// <para>8 bytes: data double</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="double">data double?</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Write(double? @double)
        {
            if (@double is null) Write(false);
            else
            {
                Write(true);
                Write((double)@double);
            }
        }

        /// <summary>
        /// Write 8 bytes of DateTime
        /// <para>Signature</para>
        /// <para>8 bytes: data dateTime</para>
        /// </summary>
        /// <param name="dateTime">data dateTime</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public unsafe void Write(DateTime dateTime)
        {
            EnsureCapacity(DateTimeSize);
            fixed (byte* ptr = _buffer.Slice(_offset, DateTimeSize)) *(DateTime*)ptr = dateTime;
            _offset += DateTimeSize;
        }

        /// <summary>
        /// Write 1 byte(nullable flag) and 8 byte(dateTime as long) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @double not null {</para>
        /// <para>8 bytes: data (long)dateTime</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="dateTime">data dateTime?</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public void Write(DateTime? dateTime)
        {
            if (dateTime is null) Write(false);
            else
            {
                Write(true);
                Write((DateTime)dateTime);
            }
        }

        /// <summary>
        /// Write 8 bytes of TimeSpan.Ticks
        /// <para>Signature</para>
        /// <para>8 bytes: data timeSpan</para>
        /// </summary>
        /// <param name="timeSpan">data timeSpan</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif        
        public unsafe void Write(TimeSpan timeSpan)
        {
            EnsureCapacity(TimeSpanSize);
            fixed (byte* ptr = _buffer.Slice(_offset, TimeSpanSize)) *(TimeSpan*)ptr = timeSpan;
            _offset += TimeSpanSize;
        }


        /// <summary>
        /// Write 1 byte(nullable flag) and 8 byte(timeSpan.Ticks) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @double not null {</para>
        /// <para>8 bytes: data (long)timeSpan.Ticks</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="timeSpan">data dateTime?</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif 
        public void Write(TimeSpan? timeSpan)
        {
            if (timeSpan is null) Write(false);
            else
            {
                Write(true);
                Write((TimeSpan)timeSpan);
            }
        }

        /// <summary>
        /// Write 16 bytes of Guid
        /// <para>Signature</para>
        /// <para>16 bytes: data guid</para>
        /// </summary>
        /// <param name="guid">data guid</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif 
        public unsafe void Write(Guid guid)
        {
            EnsureCapacity(GuidSize);
            fixed (byte* ptr = _buffer.Slice(_offset, GuidSize)) *(Guid*)ptr = guid;
            _offset += GuidSize;
        }

        /// <summary>
        /// Write 1 byte(nullable flag) and 16 byte(Guid) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @double not null {</para>
        /// <para>16 bytes: data guid</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="guid">data Guid?</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif 
        public void Write(Guid? guid)
        {
            if (guid is null) Write(false);
            else
            {
                Write(true);
                Write((Guid)guid);
            }
        }

        /// <summary>
        /// Write 16 bytes of decimal
        /// <para>Signature</para>
        /// <para>4 bytes: data decimal[0]</para>
        /// <para>4 bytes: data decimal[1]</para>
        /// <para>4 bytes: data decimal[2]</para>
        /// <para>4 bytes: data decimal[3]</para>
        /// </summary>
        /// <param name="decimal">data decimal</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif 
        public unsafe void Write(decimal @decimal)
        {
            EnsureCapacity(sizeof(decimal));
            fixed (byte* ptr = _buffer.Slice(_offset, sizeof(decimal))) *(decimal*)ptr = @decimal;
            _offset += sizeof(decimal);
        }

        /// <summary>
        /// Write 1 byte(nullable flag) and 16 byte(decimal) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @decimal not null {</para>
        /// <para>4 bytes: data decimal[0]</para>
        /// <para>4 bytes: data decimal[1]</para>
        /// <para>4 bytes: data decimal[2]</para>
        /// <para>4 bytes: data decimal[3]</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="decimal">data decimal?</param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif 
        public void Write(decimal? @decimal)
        {
            if (@decimal is null) Write(false);
            else
            {
                Write(true);
                Write((decimal)@decimal);
            }
        }

        /// <summary>
        /// Write string by char and write end of string symbol ('\0')
        /// </summary>
        /// <param name="string"></param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif 
        public void Write([NotNull]string @string)
        {
            var charArraySize = sizeof(char) * @string.Length;
            foreach (var @char in @string) Write(@char);
            Write('\0');
            _offset += charArraySize + sizeof(char);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="string"></param>
        /// <param name="encoding"></param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif 
        public void Write([NotNull]string @string, Encoding encoding)
        {
            var bytes = encoding.GetBytes(@string).AsSpan();
            EnsureCapacity(bytes.Length);
            bytes.CopyTo(_buffer.Slice(_offset, bytes.Length));
            _offset += bytes.Length;
        }

        /// <summary>
        /// Real destructor coz ref struct
        /// </summary>
        public void Dispose() => _buffer.Clear();
        #endregion

        #endregion

        #region protected (Extension class data/logic API)

        #region data

        #endregion

        #region actions

        #endregion

        #endregion

        #region internal (Core-lib class data/logic API ❢❢❢ this is only my ❢❢❢) 

        #region data

        #endregion

        #region actions

        #endregion

        #endregion

        #region private (Core-class data/logic API ❢❢❢ this is only my ❢❢❢)

        #region data


        private static readonly unsafe int DateTimeSize = sizeof(DateTime);
        private static readonly unsafe int TimeSpanSize = sizeof(TimeSpan);
        private static readonly unsafe int GuidSize = sizeof(Guid);
        private int _offset;
        private Span<byte> _buffer;

        #endregion

        #region actions

#if !UNITY
        /// <summary>
        /// TODO mb change alg for better?
        /// </summary>
        /// <param name="appendLength"></param>
        /// <exception cref="ContractException">Reached max size of buffer (16777216 bytes)</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        private void EnsureCapacity(int appendLength)
        {
            var currentLength = Buffer.Length;
            var newLength = _offset + appendLength;
            if (newLength <= currentLength) return;
            if (newLength < 256)
            {
                newLength = 256;
                FastResize(newLength);
                return;
            }
            if (newLength < currentLength * 2) newLength = currentLength * 2;
            if (newLength > 16777216) throw new ContractException("Reached max size of buffer (16777216 bytes)"); //0x1_000_000 - 3(for size 3 bytes)
            FastResize(newLength);
        }

#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        private void FastResize(int newSize)
        {
            Span<byte> newSpan = new Span<byte>(new byte[newSize], 0, newSize);
            Buffer.CopyTo(newSpan);
            //_buffer.Clear();
            _buffer = newSpan;
        }
        #endregion

        #endregion


    }
}
