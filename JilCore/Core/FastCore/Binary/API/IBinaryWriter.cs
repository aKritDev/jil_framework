﻿using System;
using System.Runtime.CompilerServices;
using System.Text;

namespace JilCore.Core.FastCore.Binary.API
{
    /// <summary>
    /// 
    /// </summary>
    public interface IBinaryWriter
    {
        /// <summary>
        /// Write data byte
        /// </summary>
        /// <param name="byte">data byte that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(byte @byte);

        /// <summary>
        /// Write data byte?
        /// </summary>
        /// <param name="byte">data byte? that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(byte? @byte);

        /// <summary>
        /// Write data sbyte
        /// </summary>
        /// <param name="sbyte">data sbyte that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(sbyte @sbyte);

        /// <summary>
        /// Write data sbyte?
        /// </summary>
        /// <param name="sbyte">data sbyte? that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(sbyte? @sbyte);

        /// <summary>
        /// Write data bool
        /// </summary>
        /// <param name="bool">data bool that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(bool @bool);

        /// <summary>
        /// Write data bool?
        /// </summary>
        /// <param name="bool">data bool? that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(bool? @bool);

        /// <summary>
        /// Write data short
        /// </summary>
        /// <param name="short">data short that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(short @short);

        /// <summary>
        /// Write data short?
        /// </summary>
        /// <param name="short">data short? that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(short? @short);

        /// <summary>
        /// Write data ushort
        /// </summary>
        /// <param name="ushort">data ushort that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(ushort @ushort);

        /// <summary>
        /// Write data ushort?
        /// </summary>
        /// <param name="ushort">data ushort? that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(ushort? @ushort);

        /// <summary>
        /// Write data char
        /// </summary>
        /// <param name="char">data char that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(char @char);

        /// <summary>
        /// Write data char?
        /// </summary>
        /// <param name="char">data char? that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(char? @char);

        /// <summary>
        /// Special. Write data uint as 3 byte (values: 0 - 16777215).
        /// <para>PS. Maybe it will be used for packet\obj max length in bytes</para>
        /// </summary>
        /// <param name="int24">data uint  that must be written by 3 bytes</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void WriteUInt24(uint int24);

        /// <summary>
        /// Write data int
        /// </summary>
        /// <param name="int">data int that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(int @int);

        /// <summary>
        /// Write data int?
        /// </summary>
        /// <param name="int">data int? that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(int? @int);

        /// <summary>
        /// Write data uint
        /// </summary>
        /// <param name="uint">data uint that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(uint @uint);

        /// <summary>
        /// Write data uint?
        /// </summary>
        /// <param name="uint">data uint? that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(uint? @uint);

        /// <summary>
        /// Write data float
        /// </summary>
        /// <param name="float">data float that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(float @float);

        /// <summary>
        /// Write data float?
        /// </summary>
        /// <param name="float">data float? that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(float? @float);

        /// <summary>
        /// Write data long
        /// </summary>
        /// <param name="long">data long that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(long @long);

        /// <summary>
        /// Write data long?
        /// </summary>
        /// <param name="long">data ulong? that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(long? @long);

        /// <summary>
        /// Write data ulong
        /// </summary>
        /// <param name="ulong">data ulong that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(ulong @ulong);

        /// <summary>
        /// Write data ulong?
        /// </summary>
        /// <param name="ulong">data ulong? that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(ulong? @ulong);

        /// <summary>
        /// Write data double
        /// </summary>
        /// <param name="double">data double that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(double @double);

        /// <summary>
        /// Write data double?
        /// </summary>
        /// <param name="double">data double? that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(double? @double);

        /// <summary>
        /// Write data DateTime
        /// </summary>
        /// <param name="dateTime">data DateTime that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(DateTime dateTime);

        /// <summary>
        /// Write data DateTime?
        /// </summary>
        /// <param name="dateTime">data DateTime? that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(DateTime? dateTime);

        /// <summary>
        /// Write data TimeSpan
        /// </summary>
        /// <param name="timeSpan">data TimeSpan that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(TimeSpan timeSpan);

        /// <summary>
        /// Write data TimeSpan?
        /// </summary>
        /// <param name="timeSpan">data TimeSpan? that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(TimeSpan? timeSpan);

        /// <summary>
        /// Write data Guid
        /// </summary>
        /// <param name="guid">data Guid that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(Guid guid);

        /// <summary>
        /// Write data Guid?
        /// </summary>
        /// <param name="guid">data Guid? that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(Guid? guid);

        /// <summary>
        /// Write data decimal
        /// </summary>
        /// <param name="decimal">data decimal that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(decimal @decimal);

        /// <summary>
        /// Write data decimal?
        /// </summary>
        /// <param name="decimal">data decimal? that must be written</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(decimal? @decimal);

        /// <summary>
        /// Write data string with end of string symbol ('\0'). This made for Lineage 2.
        /// </summary>
        /// <param name="string">data string that must be written by <see cref="Write(char)"/> with '\0' at the end</param>
        void Write(string @string);

        /// <summary>
        /// Write size and byte representation of string in chosen encoding 
        /// </summary>
        /// <param name="string">data string that must be written</param>
        /// <param name="encoding">chosen encoding</param>
#if !UNITY
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        void Write(string @string, Encoding encoding);
    }
}