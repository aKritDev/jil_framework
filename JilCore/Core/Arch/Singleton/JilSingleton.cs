﻿using System;
using System.Reflection;
using System.Threading;

namespace JilCore.Core.Arch.Singleton
{
    public abstract class JilSingleton<T> where T : JilSingleton<T>
    {
        #region public

        #region data

        #endregion

        #region methods
        public static T Instance
        {
            get
            {
                if (_instance != null) return _instance;
                Monitor.Enter(Lock);
                var temp = (T)Activator.CreateInstance(typeof(T), BindingFlags.Instance | BindingFlags.NonPublic, null, new object[] { }, null);
                Interlocked.Exchange(ref _instance, temp);
                Monitor.Exit(Lock);
                return _instance;
            }
        }
        #endregion

        #endregion

        #region private

        #region data
        // ReSharper disable once StaticMemberInGenericType
        private static readonly object Lock = new object();
        private static T _instance;
        #endregion

        #region methods

        #endregion

        #endregion

        #region protected

        #region data

        #endregion

        #region methods
        protected JilSingleton(){}
        #endregion

        #endregion
    }
}
