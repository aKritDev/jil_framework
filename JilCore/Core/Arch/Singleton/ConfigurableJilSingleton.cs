﻿using JilCore.Core.Arch.Configuration;

namespace JilCore.Core.Arch.Singleton
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TSingleton"></typeparam>
    /// <typeparam name="TConfig"></typeparam>
    public abstract class ConfigurableJilSingleton<TSingleton, TConfig> : JilSingleton<TSingleton>  
        where TSingleton : JilSingleton<TSingleton>
        where TConfig : JilConfiguration<TConfig>
    {
        #region public

        #region data

        #endregion

        #region methods

        #endregion

        #endregion

        #region private

        #region data

        #endregion

        #region methods

        #endregion

        #endregion

        #region protected

        #region data
        /// <summary>
        /// 
        /// </summary>
        public TConfig Config { get; } = JilConfiguration<TConfig>.Load();
        #endregion

        #region methods

        #endregion

        #endregion
    }
}
