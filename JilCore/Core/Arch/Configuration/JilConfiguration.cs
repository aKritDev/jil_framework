﻿// ReSharper disable StaticMemberInGenericType

using System;
using System.IO;
using System.Reflection;
using System.Text;
using JilCore.Core.Utils.FileSystem;
using Newtonsoft.Json;

namespace JilCore.Core.Arch.Configuration
{
    /// <summary>
    /// TODO добавить способы хранения конфигов - .xml, .ini, бд и тд
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class JilConfiguration<T> where  T : JilConfiguration<T>
    {
        #region public

        #region data

        #endregion

        #region methods

        #endregion

        #endregion

        #region private

        #region data

        private const string MainDirectory = "Config";
        private readonly string _dir;
        private readonly string _fileName;
        private readonly string _fullPath;
        private static T _config;

        #endregion

        #region methods

        internal static T Load()
        {
            _config = (T)Activator.CreateInstance(typeof(T), BindingFlags.Instance | BindingFlags.NonPublic, null, new object[] { }, null); //вместо new T()
            if (File.Exists(_config._fullPath))
            {
                _config = JsonConvert.DeserializeObject<T>(Encoding.UTF8.GetString(_config._fullPath.ReadFileMt()));
                return _config;
            }
            _config.Save();
            return _config;
        }

        internal void Save()
        {
            _dir.CheckDirectoryAndCreateIfNotExistsMt();
            _fullPath.WriteFileMt(JsonConvert.SerializeObject(_config, Formatting.Indented));
        }

        #endregion

        #endregion

        #region protected

        #region data

        #endregion

        #region methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subfolder"></param>
        protected JilConfiguration(string subfolder = "")
        {
            if (!string.IsNullOrEmpty(_fullPath) && !string.IsNullOrEmpty(_fileName) && !string.IsNullOrEmpty(_dir)) return;
            _dir = Path.Combine(FileSystemHelper.AssemblyDirectory, MainDirectory, subfolder); //FileSystemHelper.AssemblyDirectory
            _fileName = $"{typeof(T).Name.Replace("Config", string.Empty)}.json";
            _fullPath = Path.Combine(_dir, _fileName);
            Console.WriteLine(_fullPath);
        } 
        #endregion

        #endregion
    }
}
