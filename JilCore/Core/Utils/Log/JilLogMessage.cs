﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;

namespace JilCore.Core.Utils.Log
{
    public sealed class JilLogMessage
    {

        #region public

        #region properties

        public DateTime CreatedDate { get; }

        public string AssemblyName { get; }

        public JilLogMessageType JilLogMessageType { get; }

        public string Message { get; }

        public string MethodName { get; }

        public int LineNumber { get; }

        public string SourceFilePath { get; }

        public int ThreadId { get; }

        public Exception Exception { get; }

        #endregion

        #region methods

        public JilLogMessage(string assemblyName, JilLogMessageType jilLogMessageType, string message, string methodName, int lineNumber, string sourceFilePath, int threadId, Exception exception = null)
        {
            CreatedDate = DateTime.Now;
            JilLogMessageType = jilLogMessageType;
            AssemblyName = assemblyName;
            Message = message;
            MethodName = methodName;
            LineNumber = lineNumber;
            SourceFilePath = sourceFilePath;
            ThreadId = threadId;
            Exception = exception;
        }

        public string Build(string messageTemplate)
        {
            if (!string.IsNullOrEmpty(_fullMessage) && !string.IsNullOrEmpty(_messageTemplate) && _messageTemplate.GetHashCode() == messageTemplate.GetHashCode()) return _fullMessage;
            _messageTemplate = messageTemplate;
            _fullMessage = string.Format(_messageTemplate, CreatedDate, GetLogTypeString(JilLogMessageType), AssemblyName, Path.GetFileName(SourceFilePath)?.Replace(".cs", string.Empty), MethodName, LineNumber, Message, ThreadId, Exception);
            return _fullMessage;
        }
        #endregion

        #endregion

        #region private

        #region variable

        private string _messageTemplate;
        private string _fullMessage;

        #endregion

        #region methods
        private static string GetLogTypeString(JilLogMessageType type)
            => type switch
                {
                    JilLogMessageType.Trace => "TRACE",
                    JilLogMessageType.Debug => "DEBUG",
                    JilLogMessageType.Info => "INFO",
                    JilLogMessageType.Warn => "WARN",
                    JilLogMessageType.Error => "ERROR",
                    JilLogMessageType.Fatal => "FATAL",
                    _ => string.Empty
                };

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static StringBuilder WriteException(StringBuilder renderedMsgBuilder, Exception ex, byte recursionDepth = 0)
        {
            if (ex == null) return renderedMsgBuilder;
            renderedMsgBuilder.AppendLine($"{GetTabulator(recursionDepth)}{(recursionDepth == 0 ? "[Exception]" : "[Inner Exception]")}");
            renderedMsgBuilder.AppendLine($"{GetTabulator(recursionDepth)}{{");
            renderedMsgBuilder.Append($"{GetTabulator(recursionDepth)}\t[Exception Type]");
            renderedMsgBuilder.AppendLine($"{GetTabulator(recursionDepth)}\t\"{ex.GetType().Name}\"");
            renderedMsgBuilder.Append($"{GetTabulator(recursionDepth)}\t[Exception Message]");
            renderedMsgBuilder.AppendLine($"{GetTabulator(recursionDepth)}\t\"{ex.Message}\"");
            renderedMsgBuilder.AppendLine($"{GetTabulator(recursionDepth)}\t[Exception StackTrace]");
            renderedMsgBuilder.AppendLine($"{GetTabulator(recursionDepth)}\t\"{ex.StackTrace}\"");
            if (ex.InnerException != null) WriteException(renderedMsgBuilder, ex.InnerException, ++recursionDepth);
            renderedMsgBuilder.AppendLine($"{GetTabulator(recursionDepth)}}}");
            return renderedMsgBuilder;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static string GetTabulator(byte recursionDepth)
        {
            if (recursionDepth == 0) return string.Empty;
            var tempSb = new StringBuilder();
            for (var i = 0; i < recursionDepth; i++) tempSb.Append('\t');
            return tempSb.ToString();
        }
        #endregion

        #endregion
    }
}
