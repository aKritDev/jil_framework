﻿using System;
using System.Collections.Concurrent;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using JilCore.Core.Utils.Log.Engine;

namespace JilCore.Core.Utils.Log
{
    public sealed class JilLog
    {
        public static JilLog GetLogger(Type type)
        {
            var key = type.FullName;
            if (Instances.TryGetValue(key ?? throw new ApplicationException("TypeName cant be a null"), out var log)) return log;
            var newLogger = new JilLog(type);
            Instances.AddOrUpdate(key, newLogger, (a, b) => b);
            return newLogger;
        }

#if TRACE
        public void Trace(string message, [CallerMemberName] string methodName = null, [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string sourceFilePath = null)
            => JilLogEngine.Instance.AddMessage(new JilLogMessage(_assemblyName, JilLogMessageType.Trace, message, methodName, lineNumber, sourceFilePath, Thread.CurrentThread.ManagedThreadId));

        public void Trace(string message, Exception ex, string methodName = null, int lineNumber = 0, string sourceFilePath = null)
            => JilLogEngine.Instance.AddMessage(new JilLogMessage(_assemblyName, JilLogMessageType.Trace, message, methodName, lineNumber, sourceFilePath, Thread.CurrentThread.ManagedThreadId, ex));
#endif

#if DEBUG
        public void Debug(string message, [CallerMemberName] string methodName = null, [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string sourceFilePath = null)
            => JilLogEngine.Instance.AddMessage(new JilLogMessage(_assemblyName, JilLogMessageType.Debug, message, methodName, lineNumber, sourceFilePath, Thread.CurrentThread.ManagedThreadId));

        public void Debug(string message, Exception ex, [CallerMemberName] string methodName = null, [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string sourceFilePath = null)
            => JilLogEngine.Instance.AddMessage(new JilLogMessage(_assemblyName, JilLogMessageType.Debug, message, methodName, lineNumber, sourceFilePath, Thread.CurrentThread.ManagedThreadId, ex));
#endif

        public void Info(string message, [CallerMemberName] string methodName = null, [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string sourceFilePath = null)
            => JilLogEngine.Instance.AddMessage(new JilLogMessage(_assemblyName, JilLogMessageType.Info, message, methodName, lineNumber, sourceFilePath, Thread.CurrentThread.ManagedThreadId));

        public void Warn(string message, [CallerMemberName] string methodName = null, [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string sourceFilePath = null)
            => JilLogEngine.Instance.AddMessage(new JilLogMessage(_assemblyName, JilLogMessageType.Warn, message, methodName, lineNumber, sourceFilePath, Thread.CurrentThread.ManagedThreadId));

        public Exception Error(string message, Exception ex, [CallerMemberName] string methodName = null, [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string sourceFilePath = null)
        {
            JilLogEngine.Instance.AddMessage(new JilLogMessage(_assemblyName, JilLogMessageType.Error, message, methodName, lineNumber, sourceFilePath, Thread.CurrentThread.ManagedThreadId, ex));
            return ex;
        }

        public Exception Fatal(string message, Exception ex, [CallerMemberName] string methodName = null, [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string sourceFilePath = null)
        {
            JilLogEngine.Instance.AddMessage(new JilLogMessage(_assemblyName, JilLogMessageType.Fatal, message, methodName, lineNumber, sourceFilePath, Thread.CurrentThread.ManagedThreadId, ex));
            return ex;
        }

        private static readonly ConcurrentDictionary<string, JilLog> Instances = new ConcurrentDictionary<string, JilLog>();

        private readonly string _assemblyName;

        private JilLog(Type type) => _assemblyName = Assembly.GetAssembly(type).GetName().Name;
    }
}
