﻿namespace JilCore.Core.Utils.Log
{
    public enum JilLogMessageType
    {
        All,
        Trace,
        Debug,
        Info,
        Warn,
        Error,
        Fatal,
    }
}
