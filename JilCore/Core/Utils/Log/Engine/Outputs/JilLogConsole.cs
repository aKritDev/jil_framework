﻿using System;

namespace JilCore.Core.Utils.Log.Engine.Outputs
{
    internal sealed class JilLogConsoleConfig : JilLogOutputConfig<JilLogConsoleConfig>
    {
        private JilLogConsoleConfig() { }

        public string[] Template { get; set; } =
        {
            "[{0}][{1}][{2}.dll({3}.{4}():{5})]:\"{6}.\"<Thread:{7}>", //All
            "[{0}][{1}][{2}.dll({3}.{4}():{5})]:\"{6}.\"<Thread:{7}>", //Trace
            "[{0}][{1}][{2}.dll({3}.{4}():{5})]:\"{6}.\"<Thread:{7}>", //Debug
            "[{0}][{1}]:\"{6}.\"", //Info
            "[{0}][{1}][{2}.dll({3}.{4}():{5})]:\"{6}.\"<Thread:{7}>", //Warn
            "[{0}][{1}][{2}.dll({3}.{4}():{5})]:\"{6}.\"<Thread:{7}>\r{8}", //Error
            "[{0}][{1}][{2}.dll({3}.{4}():{5})]:\"{6}.\"<Thread:{7}>\r{8}" //Fatal
        };

        public bool UseColorSystem { get; set; } = true;
        public ConsoleColor TraceColor { get; set; } = ConsoleColor.DarkGray;
        public ConsoleColor DebugColor { get; set; } = ConsoleColor.White;
        public ConsoleColor InfoColor { get; set; } = ConsoleColor.Green;
        public ConsoleColor WarnColor { get; set; } = ConsoleColor.Yellow;
        public ConsoleColor ErrorColor { get; set; } = ConsoleColor.Red;
        public ConsoleColor FatalColor { get; set; } = ConsoleColor.DarkRed;
    }

    internal sealed class JilLogConsole : JilLogOutput<JilLogConsole, JilLogConsoleConfig>
    {
        private JilLogConsole(){}

        internal override void Processing(JilLogMessage message)
        {
            var oldColor = Console.ForegroundColor;
            if (Config.UseColorSystem)
                // ReSharper disable once SwitchStatementMissingSomeCases
                switch (message.JilLogMessageType)
                {
                    case JilLogMessageType.Trace: Console.ForegroundColor = Config.TraceColor; break;
                    case JilLogMessageType.Debug: Console.ForegroundColor = Config.DebugColor; break;
                    case JilLogMessageType.Info: Console.ForegroundColor = Config.InfoColor; break;
                    case JilLogMessageType.Warn: Console.ForegroundColor = Config.WarnColor; break;
                    case JilLogMessageType.Error: Console.ForegroundColor = Config.ErrorColor; break;
                    case JilLogMessageType.Fatal: Console.ForegroundColor = Config.FatalColor; break;
                }
            Console.WriteLine(message.Build(Config.Template[(int)message.JilLogMessageType]));
            Console.ForegroundColor = oldColor;
        }

        internal override void Push(){}
    }
}
