﻿using System;

namespace JilCore.Core.Utils.Log.Engine.Outputs
{
    [Flags]
    public enum JilLogOutputTypes
    {
        Console,
        FileSystem,
        Email,
        Socket,
        Db
    }
}
