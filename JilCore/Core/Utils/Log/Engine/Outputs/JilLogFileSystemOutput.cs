﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using static JilCore.Core.Utils.FileSystem.FileSystemHelper;

namespace JilCore.Core.Utils.Log.Engine.Outputs
{
    internal sealed class JilLogFileSystemOutputConfig : JilLogOutputConfig<JilLogFileSystemOutputConfig>
    {
        private JilLogFileSystemOutputConfig() { }

        public string[] Template { get; set; } =
        {
            "[{0}][{1}][{2}.dll({3}.{4}():{5})]:\"{6}.\"<Thread:{7}>", //All
            "[{0}][{1}][{2}.dll({3}.{4}():{5})]:\"{6}.\"<Thread:{7}>", //Trace
            "[{0}][{1}][{2}.dll({3}.{4}():{5})]:\"{6}.\"<Thread:{7}>", //Debug
            "[{0}][{1}]:\"{6}.\"", //Info
            "[{0}][{1}][{2}.dll({3}.{4}():{5})]:\"{6}.\"<Thread:{7}>", //Warn
            "[{0}][{1}][{2}.dll({3}.{4}():{5})]:\"{6}.\"<Thread:{7}>\r{8}", //Error
            "[{0}][{1}][{2}.dll({3}.{4}():{5})]:\"{6}.\"<Thread:{7}>\r{8}" //Fatal
        };

        public override int PushOnTick { get; set; } = 250;
    }

    internal sealed class JilLogFileSystemOutput : JilLogOutput<JilLogFileSystemOutput, JilLogFileSystemOutputConfig>
    {
        #region public

        #region data

        #endregion

        #region methods

        #endregion

        #endregion

        #region private

        #region data
        private static readonly string[] MonthsNames =
        {
            "January", "February", "March", "April", "May", "June", "July",
            "August", "September", "October", "November", "December"
        };
        private DateTime _currentDateTime;
        private string _logsCurrentFileSystemPath;
        private Dictionary<JilLogMessageType, string> _logFileFullNames;
        private readonly Dictionary<JilLogMessageType, StringBuilder> _stringBuilders = new Dictionary<JilLogMessageType, StringBuilder>
        {
            { JilLogMessageType.All, new StringBuilder() },
            { JilLogMessageType.Trace, new StringBuilder() },
            { JilLogMessageType.Debug, new StringBuilder() },
            { JilLogMessageType.Info, new StringBuilder() },
            { JilLogMessageType.Warn, new StringBuilder() },
            { JilLogMessageType.Error, new StringBuilder() },
            { JilLogMessageType.Fatal, new StringBuilder() }
        };
        #endregion

        #region methods

        private JilLogFileSystemOutput() {}

        internal override void Processing(JilLogMessage message)
        {
            var builtMsg = message.Build(message.Build(Config.Template[(int) message.JilLogMessageType]));
            _stringBuilders[JilLogMessageType.All].AppendLine(builtMsg);
            _stringBuilders[message.JilLogMessageType].AppendLine(builtMsg);
        }

        internal override void Push()
        {
            CheckDateFiles();
            foreach (var (key, value) in _logFileFullNames)
            {
                var sb = _stringBuilders[key];
                value.AppendToFileMt(sb.ToString());
                sb.Clear();
            }
            CurrentTick = 0;
        }

        private void CheckDateFiles()
        {
            var currentDate = DateTime.Now;
            if (_currentDateTime.Date == currentDate.Date) return;
            _currentDateTime = currentDate;
            GenerateNewDateFiles();
        }

        private void GenerateNewDateFiles()
        {
            var logTimeStr = $"{_currentDateTime:dd_MM_yyyy}";
            _logsCurrentFileSystemPath = Path.Combine(AssemblyDirectory, "Logs",
                _currentDateTime.Year.ToString(),
                $"{MonthsNames[_currentDateTime.Month - 1]}({_currentDateTime.Month})",
                _currentDateTime.Day.ToString()).Replace("file:\\", string.Empty);
            if (!Directory.Exists(_logsCurrentFileSystemPath)) Directory.CreateDirectory(_logsCurrentFileSystemPath);
            _logFileFullNames = new Dictionary<JilLogMessageType, string>
            {
                {JilLogMessageType.All, Path.Combine(_logsCurrentFileSystemPath, $"{logTimeStr}-all.log")},
                {JilLogMessageType.Trace, Path.Combine(_logsCurrentFileSystemPath, $"{logTimeStr}-trace.log")},
                {JilLogMessageType.Debug, Path.Combine(_logsCurrentFileSystemPath, $"{logTimeStr}-debug.log")},
                {JilLogMessageType.Info, Path.Combine(_logsCurrentFileSystemPath, $"{logTimeStr}-info.log")},
                {JilLogMessageType.Warn, Path.Combine(_logsCurrentFileSystemPath, $"{logTimeStr}-warning.log")},
                {JilLogMessageType.Error, Path.Combine(_logsCurrentFileSystemPath, $"{logTimeStr}-error.log")},
                {JilLogMessageType.Fatal, Path.Combine(_logsCurrentFileSystemPath, $"{logTimeStr}-fatal.log")}
            };
            foreach (var stream in from logFileFullName in _logFileFullNames.Values where !File.Exists(logFileFullName) select File.Create(logFileFullName)) stream.Dispose();
        }
        #endregion

        #endregion



    }
}
