﻿using JilCore.Core.Arch.Configuration;
using JilCore.Core.Arch.Singleton;

namespace JilCore.Core.Utils.Log.Engine.Outputs
{
    public abstract class JilLogOutputConfig<T> : JilConfiguration<T> 
        where T : JilLogOutputConfig<T>
    {
        protected JilLogOutputConfig(): base("Log") { }

        public virtual int PushOnTick { get; set; } = 200;
    }

    public abstract class JilLogOutput<TOutput, TConfig> : ConfigurableJilSingleton<TOutput, TConfig> 
        where TOutput : JilLogOutput<TOutput, TConfig>
        where TConfig : JilLogOutputConfig<TConfig>
    {
        #region public

        #region data

        #endregion

        #region methods

        #endregion

        #endregion

        #region private

        #region data
        internal int CurrentTick;
        #endregion

        #region methods

        #endregion

        #endregion

        #region protected

        #region data

        #endregion

        #region methods

        internal abstract void Processing(JilLogMessage message);

        internal abstract void Push();


        #endregion

        #endregion
    }
}
