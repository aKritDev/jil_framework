﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Timers;
using JilCore.Core.Arch.Configuration;
using JilCore.Core.Arch.Singleton;
using JilCore.Core.Utils.Log.Engine.Outputs;

namespace JilCore.Core.Utils.Log.Engine
{

    internal sealed class JilLogEngineConfig : JilConfiguration<JilLogEngineConfig>
    {
        /// <summary>
        /// Задержка каждого тика в милисекундах
        /// </summary>
        public int TickInterval { get; set; } = 200;

        /// <summary>
        /// Предел сообщений, обрабатываемых за 1 тик
        /// </summary>
        public int MaxMessagesProcessingPerTick { get; set; } = 10_000;

        /// <summary>
        /// Выходы логгера
        /// </summary>
        public JilLogOutputTypes OutputTypes { get; set; } = JilLogOutputTypes.Console | JilLogOutputTypes.FileSystem;

        private JilLogEngineConfig() : base("Log") { }
    }

    internal sealed class JilLogEngine : ConfigurableJilSingleton<JilLogEngine, JilLogEngineConfig>
    {
        #region public

        #region properties

        #endregion

        #region methods

        private JilLogEngine()
        {
            _logMessages = new ConcurrentQueue<JilLogMessage>();
            _outputs = new List<dynamic>();
            // ReSharper disable once NonConstantEqualityExpressionHasConstantResult
            if ((Config.OutputTypes & JilLogOutputTypes.Console) == JilLogOutputTypes.Console) _outputs.Add(JilLogConsole.Instance);
            if ((Config.OutputTypes & JilLogOutputTypes.FileSystem) == JilLogOutputTypes.FileSystem) _outputs.Add(JilLogFileSystemOutput.Instance);
            /*
            if ((Config.OutputTypes & JilLogOutputTypes.Email) == JilLogOutputTypes.Email) _outputs.Add(JilLogConsole.Instance);
            if ((Config.OutputTypes & JilLogOutputTypes.Socket) == JilLogOutputTypes.Socket) _outputs.Add(JilLogConsole.Instance);
            if ((Config.OutputTypes & JilLogOutputTypes.Db) == JilLogOutputTypes.Db) _outputs.Add(JilLogConsole.Instance);*/
            _logWorkerTimer = new Timer(Config.TickInterval);
            _logWorkerTimer.Elapsed += JilLogWorker;
            _logWorkerTimer.Start();
        }

        public void AddMessage(JilLogMessage message)
        {
            if (message.JilLogMessageType != JilLogMessageType.Fatal) _logMessages.Enqueue(message);
            else
            {
                foreach (var output in _outputs)
                {
                    output.Processing(message);
                    output.Push();
                }
            }
        }

        #endregion

        #endregion

        #region private

        #region variables

        private readonly List<dynamic> _outputs; //Пускай пока так. потому что generics а обобщение для удобного во всех смыслах конфига
        private readonly ConcurrentQueue<JilLogMessage> _logMessages;
        private readonly Timer _logWorkerTimer;

        #endregion

        #region methods

        private void JilLogWorker(object sender, ElapsedEventArgs e)
        {
            _logWorkerTimer.Stop();
            var count = _logMessages.Count;
            count = count > Config.MaxMessagesProcessingPerTick ? Config.MaxMessagesProcessingPerTick : count;
            for (var i = 0; i < count; i++) //Обрабатываем
            {
                if (!_logMessages.TryDequeue(out var currentMgs)) continue;
                foreach (var output in _outputs) output.Processing(currentMgs);
            }
            foreach (var output in _outputs) //Пушим
            {
                output.CurrentTick++;
                if (output.CurrentTick < output.Config.PushOnTick) continue;
                output.Push();
                output.CurrentTick = 0;
            }
            _logWorkerTimer.Start();
        }

        #endregion

        #endregion

    }
}
