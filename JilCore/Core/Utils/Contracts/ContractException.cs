﻿using System;

namespace JilCore.Core.Utils.Contracts
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class ContractException : ApplicationException
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public ContractException(string message) : base(message) { }
    }
}
