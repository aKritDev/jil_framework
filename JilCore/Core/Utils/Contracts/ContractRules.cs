﻿using System;
using System.Runtime.CompilerServices;

namespace JilCore.Core.Utils.Contracts
{
    /// <summary>
    /// 
    /// </summary>
    public static class ContractRules
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="current"></param>
        /// <param name="limit"></param>
        /// <param name="message"></param>
        /// <exception cref="ApplicationException"></exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void CantBeMoreThen(this int current, int limit, string message)
        {
            if (current > limit) throw new ContractException(message);
        }
    }
}
