﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;

namespace JilCore.Core.Utils.FileSystem
{
    public static class FileSystemHelper
    {
        private static readonly ConcurrentDictionary<string, object> DirLockers = new ConcurrentDictionary<string, object>();

        /// <summary>
        /// Пишет байты в файл
        /// <para>Thread-safe? TODO (File.Delete -> need check)
        /// </para>
        /// </summary>
        /// <param name="fullFileName"></param>
        /// <param name="bytes"></param>
        public static void WriteFileMt(this string fullFileName, byte[] bytes)
        {
            if (File.Exists(fullFileName)) File.Delete(fullFileName);
            using var bw = new BinaryWriter(new FileStream(fullFileName, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite));
            bw.Write(bytes);
        }

        /// <summary>
        /// Пишет текст в файл в кодировке UTF-8.
        /// <para>Thread-safe</para>
        /// </summary>
        /// <param name="fullFileName"></param>
        /// <param name="text"></param>
        public static void WriteFileMt(this string fullFileName, string text) => WriteFileMt(fullFileName, Encoding.UTF8.GetBytes(text)); //TODO performance improve (нужен ли лишний call)

        /// <summary>
        /// Пишет текст в файл в указанной кодировке
        /// <para>Thread-safe? TODO (File.Delete -> need check)
        /// </para>
        /// </summary>
        /// <param name="fullFileName"></param>
        /// <param name="text"></param>
        /// <param name="encoding"></param>
        public static void WriteFileMt(this string fullFileName, string text, Encoding encoding)
        {
            if (File.Exists(fullFileName)) File.Delete(fullFileName);
            using var bw = new BinaryWriter(new FileStream(fullFileName, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite), encoding);
            bw.Write(text);
        }

        /// <summary>
        /// Добавляет к файлу байты.
        /// <para>Thread-safe</para>
        /// </summary>
        /// <param name="fullFileName"></param>
        /// <param name="bytes"></param>
        public static void AppendToFileMt(this string fullFileName, byte[] bytes)
        {
            using var bw = new BinaryWriter(new FileStream(fullFileName, FileMode.Append, FileAccess.Write, FileShare.ReadWrite));
            bw.Write(bytes);
        }

        /// <summary>
        /// Добавляет к файлу текст в кодировке UTF-8.
        /// <para>Thread-safe</para>
        /// </summary>
        /// <param name="fullFileName"></param>
        /// <param name="text"></param>
        public static void AppendToFileMt(this string fullFileName, string text)
        {
            using var bw = new BinaryWriter(new FileStream(fullFileName, FileMode.Append, FileAccess.Write, FileShare.ReadWrite), Encoding.Unicode);
            bw.Write(text);
        }

        /// <summary>
        /// Добавляет к файлу текст в указанной кодировке
        /// <para>Thread-safe</para>
        /// </summary>
        /// <param name="fullFileName"></param>
        /// <param name="text"></param>
        /// <param name="encoding"></param>
        public static void AppendToFileMt(this string fullFileName, string text, Encoding encoding)
        {
            using var bw = new BinaryWriter(new FileStream(fullFileName, FileMode.Append, FileAccess.Write, FileShare.ReadWrite), encoding);
            bw.Write(text);
        }

        /// <summary>
        /// Читает байты из файла
        /// <para>Thread-safe</para>
        /// </summary>
        /// <param name="fullFileName"></param>
        /// <returns></returns>
        public static byte[] ReadFileMt(this string fullFileName)
        {
            using var fileStream = new FileStream(fullFileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            using var br = new BinaryReader(fileStream);
            return br.ReadBytes((int)fileStream.Length);
        }


        public static void CheckDirectoryAndCreateIfNotExists(this string directory)
        {
            if (!Directory.Exists(directory)) Directory.CreateDirectory(directory);
        }

        /// <summary>
        /// Проверет имеется ли дирректория по указанному пути, если нет, создает ее.
        /// <para>Tread-safe</para>
        /// </summary>
        /// <param name="directory"></param>
        public static void CheckDirectoryAndCreateIfNotExistsMt(this string directory)
        {
            if (!DirLockers.TryGetValue(directory, out var locker))
            {
                locker = new object();
                DirLockers.AddOrUpdate(directory, locker, (s, o) => o);
            }
            Monitor.Enter(locker);
            var isExists = Directory.Exists(directory);
            Monitor.Exit(locker);
            if (!isExists) Directory.CreateDirectory(directory);
        }

#if IsNative
        /// <summary>
        /// Получает псевдопуть до ReadyToRun app
        /// </summary>
        public static string AssemblyDirectory => Directory.GetCurrentDirectory();
#else
        public static string AssemblyDirectory => Path.GetDirectoryName(Uri.UnescapeDataString(new UriBuilder(Assembly.GetExecutingAssembly().CodeBase).Path));
#endif
    }
}
