﻿using System.Collections.Concurrent;
using System.Reflection;
using System.Reflection.Emit;
using JilCore.Core.FastCore.Binary;

namespace JilCore.Core.Utils.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class JilObjectSerializerExtensions
    {
        private delegate byte[] TestDynamic<in T>(T currentObject, BinaryWriterDynamicSize writer = default);
        private static readonly ConcurrentDictionary<string, dynamic> CompiledMethods = new ConcurrentDictionary<string, dynamic>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static byte[] ToBytes<T>(this T obj)
        {
            var objTypeName = obj.GetType().Name;
            if (CompiledMethods.TryGetValue(objTypeName, out var compiledMethod))
            {
                var temp = (TestDynamic<T>) compiledMethod;
                return temp(obj);
            }
            var compiledMethodNew = GenerateTestDynamic<T>();
            CompiledMethods.TryAdd(objTypeName, compiledMethodNew);
            return compiledMethodNew(obj);
        }

        private static TestDynamic<T> GenerateTestDynamic<T>()
        {
            var division = new DynamicMethod("ObjectWriterMethod",
                MethodAttributes.Public | MethodAttributes.Static,
                CallingConventions.Standard,
                typeof(byte[]),
                new[]
                {
                    typeof(T),
                    typeof(BinaryWriterDynamicSize)
                },
                typeof(JilObjectSerializerExtensions),
                true);
            var il = division.GetILGenerator();
            foreach (var propertyInfo in typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                #region ValueType
                il.Emit(OpCodes.Ldarga_S, 1); // writer
                il.Emit(OpCodes.Ldarg_0); // currentObj
                il.Emit(OpCodes.Callvirt, propertyInfo.GetMethod);
                var method = typeof(BinaryWriterDynamicSize).GetMethod(
                    "Write",
                    BindingFlags.Public | BindingFlags.Instance,
                    null,
                    new[] {propertyInfo.PropertyType},
                    null);
                il.Emit(OpCodes.Call, method);
                #endregion
            }
            il.Emit(OpCodes.Ldarga_S, 1); // writer
            il.Emit(OpCodes.Call, typeof(BinaryWriterDynamicSize).GetProperty("Buffer")?.GetMethod);
            il.Emit(OpCodes.Ret);
            return (TestDynamic<T>)division.CreateDelegate(typeof(TestDynamic<T>));
        }
    }
}
