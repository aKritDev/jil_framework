﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace JilCore.Core.Utils.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class StopwatchExtension
    {
        /// <summary>
        ///     Возвращает время выполнения кода в наносекундах.
        /// </summary>
        /// <param name="watch"></param>
        /// <returns></returns>
        public static long ElapsedNanoSeconds(this Stopwatch watch) => watch.ElapsedTicks * 1000000000 / Stopwatch.Frequency;
        /// <summary>
        ///     Возвращает время выполнения кода в микросекундах.
        /// </summary>
        /// <param name="watch"></param>
        /// <returns></returns>
        public static long ElapsedMicroSeconds(this Stopwatch watch) => watch.ElapsedTicks * 1000000 / Stopwatch.Frequency;
    }
}
