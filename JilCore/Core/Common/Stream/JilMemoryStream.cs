﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace JilCore.Core.Common.Stream
{
    /// <summary>
    /// 
    /// </summary>
    public class JilMemoryStream : System.IO.Stream
    {
        private byte[] _buffer;
        private int _origin;
        private int _position;
        private int _length;
        private int _capacity;
        private bool _expandable;
        private bool _writable;
        private bool _exposable;
        private bool _isOpen;
        private Task<int> _lastReadTask;

        /// <summary>
        /// 
        /// </summary>
        public JilMemoryStream() : this(0) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="capacity"></param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public JilMemoryStream(int capacity)
        {
            _buffer = capacity != 0 ? new byte[capacity] : Array.Empty<byte>();
            _capacity = capacity;
            _expandable = true;
            _writable = true;
            _exposable = true;
            _origin = 0;
            _isOpen = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        public JilMemoryStream(byte[] buffer) : this(buffer, true) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="writable"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public JilMemoryStream(byte[] buffer, bool writable)
        {
            _buffer = buffer;
            _length = _capacity = buffer.Length;
            _writable = writable;
            _exposable = false;
            _origin = 0;
            _isOpen = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="index"></param>
        /// <param name="count"></param>
        public JilMemoryStream(byte[] buffer, int index, int count) : this(buffer, index, count, true, false) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="index"></param>
        /// <param name="count"></param>
        /// <param name="writable"></param>
        public JilMemoryStream(byte[] buffer, int index, int count, bool writable) : this(buffer, index, count, writable, false) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="index"></param>
        /// <param name="count"></param>
        /// <param name="writable"></param>
        /// <param name="publiclyVisible"></param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        /// <exception cref="ArgumentException"></exception>
        public JilMemoryStream(byte[] buffer, int index, int count, bool writable, bool publiclyVisible)
        {
            //if (buffer.Length - index < count) throw new ArgumentException(SR.Argument_InvalidOffLen);
            _buffer = buffer;
            _origin = _position = index;
            _length = _capacity = index + count;
            _writable = writable;
            _exposable = publiclyVisible;
            _expandable = false;
            _isOpen = true;
        }

        /// <summary>
        /// 
        /// </summary>
        public override bool CanRead => _isOpen;

        /// <summary>
        /// 
        /// </summary>
        public override bool CanSeek => _isOpen;

        /// <summary>
        /// 
        /// </summary>
        public override bool CanWrite => _writable;

        private void EnsureNotClosed()
        {
            //if (!this._isOpen) throw Error.GetStreamIsClosed();
        }

        private void EnsureWriteable()
        {
            //if (!this.CanWrite) throw Error.GetWriteNotSupported();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (!disposing) return;
                _isOpen = false;
                _writable = false;
                _expandable = false;
                _lastReadTask = null;
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        private bool EnsureCapacity(int value)
        {
            //if (value < 0)  throw new IOException(SR.IO_StreamTooLong);
            if (value <= _capacity) return false;
            var num = Math.Max(value, 256);
            if (num < _capacity * 2) num = _capacity * 2;
            if ((uint)(_capacity * 2) > 2147483591U) num = Math.Max(value, 2147483591);
            Capacity = num;
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Flush()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override Task FlushAsync(CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested) return Task.FromCanceled(cancellationToken);
            try
            {
                Flush();
                return Task.CompletedTask;
            }
            catch (Exception ex)
            {
                return Task.FromException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual byte[] GetBuffer()
        {
            //if (!this._exposable) throw new UnauthorizedAccessException(SR.UnauthorizedAccess_MemStreamBuffer);
            return this._buffer;
        }

        public virtual bool TryGetBuffer(out ArraySegment<byte> buffer)
        {
            if (!_exposable)
            {
                buffer = new ArraySegment<byte>();
                return false;
            }
            buffer = new ArraySegment<byte>(_buffer, _origin, _length - _origin);
            return true;
        }

        internal byte[] InternalGetBuffer() => _buffer;


        internal int InternalGetPosition() => _position;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal ReadOnlySpan<byte> InternalReadSpan(int count)
        {
            EnsureNotClosed();
            var position = _position;
            var num = position + count;
            if ((uint)num > (uint)_length)
            {
                _position = _length;
                //throw Error.GetEndOfFile();
                return null;
            }
            var readOnlySpan = new ReadOnlySpan<byte>(_buffer, position, count);
            _position = num;
            return readOnlySpan;
        }

        internal int InternalEmulateRead(int count)
        {
            this.EnsureNotClosed();
            int num = this._length - this._position;
            if (num > count)
                num = count;
            if (num < 0)
                num = 0;
            this._position += num;
            return num;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        /// <exception cref="NotSupportedException"></exception>
        public virtual int Capacity
        {
            get
            {
                EnsureNotClosed();
                return _capacity - _origin;
            }
            set
            {
                //if (value < Length) throw new ArgumentOutOfRangeException(nameof(value), SR.ArgumentOutOfRange_SmallCapacity);
                EnsureNotClosed();
                //if (!_expandable && value != Capacity) throw new NotSupportedException(SR.NotSupported_MemStreamNotExpandable);
                if (!_expandable || value == _capacity) return;
                if (value > 0)
                {
                    var numArray = new byte[value];
                    if (_length > 0) Buffer.BlockCopy(_buffer, 0, numArray, 0, _length);
                    _buffer = numArray;
                }
                else _buffer = Array.Empty<byte>();
                _capacity = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override long Length
        {
            get
            {
                EnsureNotClosed();
                return _length - _origin;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public override long Position
        {
            get
            {
                EnsureNotClosed();
                return _position - _origin;
            }
            set
            {
                //if (value < 0L) throw new ArgumentOutOfRangeException(nameof(value), SR.ArgumentOutOfRange_NeedNonNegNum);
                EnsureNotClosed();
                //if (value > int.MaxValue) throw new ArgumentOutOfRangeException(nameof(value), SR.ArgumentOutOfRange_StreamLength);
                _position = _origin + (int)value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer">cant be null</param>
        /// <param name="offset">always 0</param>
        /// <param name="count"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        /// <exception cref="ArgumentException"></exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override int Read(byte[] buffer, int offset, int count)
        {
            //if (buffer == null) throw new ArgumentNullException(nameof(buffer), SR.ArgumentNull_Buffer);
            //if (offset < 0) throw new ArgumentOutOfRangeException(nameof(offset), SR.ArgumentOutOfRange_NeedNonNegNum);
            //if (count < 0) throw new ArgumentOutOfRangeException(nameof(count), SR.ArgumentOutOfRange_NeedNonNegNum);
            //if (buffer.Length - offset < count) throw new ArgumentException("Out of range");
            //EnsureNotClosed();
            var count1 = _length - _position;
            if (count1 > count) count1 = count;
            if (count1 <= 0) return 0;
            if (count1 <= 8)
            {
                var num = count1;
                while (--num >= 0) buffer[offset + num] = _buffer[_position + num];
            }
            else Buffer.BlockCopy(_buffer, _position, buffer, offset, count1);
            _position += count1;
            return count1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public override int Read(Span<byte> buffer)
        {
           // if (GetType() != typeof(JilMemoryStream)) return base.Read(buffer);
            //EnsureNotClosed();
            var length = Math.Min(_length - _position, buffer.Length);
            if (length <= 0) return 0;
            new Span<byte>(_buffer, _position, length).CopyTo(buffer);
            _position += length;
            return length;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int ReadByte()
        {
            //EnsureNotClosed();
            if (_position >= _length) return -1;
            return _buffer[_position++];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="destination"></param>
        /// <param name="bufferSize"></param>
        public override void CopyTo(System.IO.Stream destination, int bufferSize)
        {
            //StreamHelpers.ValidateCopyToArgs((System.IO.Stream)this, destination, bufferSize);
            //if (GetType() != typeof(JilMemoryStream)) base.CopyTo(destination, bufferSize);
            var position = _position;
            var count = InternalEmulateRead(_length - position);
            if (count <= 0) return;
            destination.Write(_buffer, position, count);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="loc"></param>
        /// <returns></returns>
        /// <exception cref="IOException"></exception>
        /// <exception cref="ArgumentException"></exception>
        public override long Seek(long offset, SeekOrigin loc)
        {
            EnsureNotClosed();
            //if (offset > (long)int.MaxValue) throw new ArgumentOutOfRangeException(nameof(offset), SR.ArgumentOutOfRange_StreamLength);
            switch (loc)
            {
                case SeekOrigin.Begin:
                    var num1 = _origin + (int)offset;
                    //if (offset < 0L || num1 < _origin) throw new IOException(SR.IO_SeekBeforeBegin);
                    _position = num1;
                    break;
                case SeekOrigin.Current:
                    var num2 = _position + (int)offset;
                    //if (_position + offset < _origin || num2 < _origin) throw new IOException(SR.IO_SeekBeforeBegin);
                    _position = num2;
                    break;
                case SeekOrigin.End:
                    var num3 = _length + (int)offset;
                    //if (_length + offset < _origin || num3 < _origin) throw new IOException(SR.IO_SeekBeforeBegin);
                    _position = num3;
                    break;
                //default: throw new ArgumentException(SR.Argument_InvalidSeekOrigin);
            }
            return _position;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public override void SetLength(long value)
        {
            //if (value < 0L || value > (long)int.MaxValue) throw new ArgumentOutOfRangeException(nameof(value), SR.ArgumentOutOfRange_StreamLength);
            EnsureWriteable();
            //if (value > int.MaxValue - _origin) throw new ArgumentOutOfRangeException(nameof(value), SR.ArgumentOutOfRange_StreamLength);
            var num = _origin + (int)value;
            if (!EnsureCapacity(num) && num > _length)  Array.Clear(_buffer, _length, num - _length);
            _length = num;
            if (_position <= num) return;
            _position = num;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual byte[] ToArray()
        {
            var count = _length - _origin;
            if (count == 0) return Array.Empty<byte>();
            var numArray = new byte[count];
            Buffer.BlockCopy(_buffer, _origin, numArray, 0, count);
            return numArray;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="IOException"></exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override void Write(byte[] buffer, int offset, int count)
        {
            //if (buffer == null) throw new ArgumentNullException(nameof(buffer), SR.ArgumentNull_Buffer);
            //if (offset < 0) throw new ArgumentOutOfRangeException(nameof(offset), SR.ArgumentOutOfRange_NeedNonNegNum);
            //if (count < 0) throw new ArgumentOutOfRangeException(nameof(count), SR.ArgumentOutOfRange_NeedNonNegNum);
            //if (buffer.Length - offset < count)  throw new ArgumentException(SR.Argument_InvalidOffLen);
            //EnsureNotClosed();
            //EnsureWriteable();
            var futurePos = _position + count;
            //if (num1 < 0) throw new IOException(SR.IO_StreamTooLong);
            if (futurePos > _length)
            {
                var flag = _position > _length;
                if (futurePos > _capacity && EnsureCapacity(futurePos)) flag = false;
                if (flag) Array.Clear(_buffer, _length, futurePos - _length);
                _length = futurePos;
            }
            if (count <= 8 && buffer != _buffer)
            {
                var num2 = count;
                while (--num2 >= 0) _buffer[_position + num2] = buffer[offset + num2];
            }
            else Buffer.BlockCopy(buffer, offset, _buffer, _position, count);
            _position = futurePos;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        /// <exception cref="IOException"></exception>
        public override void Write(ReadOnlySpan<byte> buffer)
        {
            //if (GetType() != typeof(JilMemoryStream)) base.Write(buffer);
            //EnsureNotClosed();
            //EnsureWriteable();
            var num = _position + buffer.Length;
            //if (num < 0) throw new IOException(SR.IO_StreamTooLong);
            if (num > _length)
            {
                var flag = _position > _length;
                if (num > _capacity && EnsureCapacity(num)) flag = false;
                if (flag) Array.Clear(_buffer, _length, num - _length);
                _length = num;
            }
            buffer.CopyTo(new Span<byte>(_buffer, _position, buffer.Length));
            _position = num;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public override void WriteByte(byte value)
        {
            //EnsureNotClosed();
            //EnsureWriteable();
            if (_position >= _length)
            {
                var num = _position + 1;
                var flag = _position > _length;
                if (num >= _capacity && EnsureCapacity(num)) flag = false;
                if (flag) Array.Clear(_buffer, _length, _position - _length);
                _length = num;
            }
            _buffer[_position++] = value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public virtual void WriteTo(System.IO.Stream stream)
        {
            //if (stream == null) throw new ArgumentNullException(nameof(stream), SR.ArgumentNull_Stream);
            EnsureNotClosed();
            stream.Write(_buffer, _origin, _length - _origin);
        }
    }
}
