﻿using JilCore.Core.Utils.Contracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;

namespace JilCore.Core.Common.Stream
{
    /// <summary>
    /// <para>This class made for simple bytes read\write</para>
    /// <para>PS. Wanna say thx to developers pirate servers Lineage II:</para>
    /// <para>L2JServer.com for great work</para>
    /// <para>Jake Song for mind</para>
    /// </summary>
    public class JilByteStreamWrapper : IDisposable
    {
        #region public (Open class data/logic API ▼▼▼)

        #region data

        #endregion

        #region actions

        /// <summary>
        /// Creates instance of JilByteStreamWrapper like improved <see cref="MemoryStream"/>
        /// </summary>
        public JilByteStreamWrapper(int capacity) => _wrappedStream = new MemoryStream(capacity);


        /// <summary>
        /// Creates instance of JilByteStreamWrapper like improved <see cref="MemoryStream"/> from byte array
        /// </summary>
        /// <param name="buffer"></param>
        /// <exception cref="T:System.ArgumentNullException"><paramref name="buffer">buffer</paramref> is null.</exception>
        public JilByteStreamWrapper(byte[] buffer) => _wrappedStream = new MemoryStream(buffer);

        /// <summary>
        /// Creates instance of JilByteStreamWrapper like improved <see cref="System.IO.Stream"/>
        /// </summary>
        /// <param name="wrappedStream"></param>
        public JilByteStreamWrapper(System.IO.Stream wrappedStream) => _wrappedStream = wrappedStream;

        /// <summary>
        /// Close stream
        /// </summary>
        public void Dispose() => _wrappedStream.Close();

        #region Write

        /// <summary>
        /// Write 1 data byte
        /// </summary>
        /// <param name="byte">data byte</param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing, or the stream is already closed.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(byte @byte) => _wrappedStream.WriteByte(@byte);

        /// <summary>
        /// Write 1 byte(nullable flag) and 1 byte(@byte) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @byte not null {</para>
        /// <para>1 byte: data byte</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="byte">data byte</param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing, or the stream is already closed.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(byte? @byte)
        {
            if (@byte is null) Write(false);
            else
            {
                Write(true);
                Write((byte)@byte);
            }
        }

        /// <summary>
        /// Write 1 data (byte)sbyte
        /// <para>Signature</para>
        /// <para>1 byte: data (byte)sbyte </para>
        /// </summary>
        /// <param name="sbyte">data sbyte</param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing, or the stream is already closed.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(sbyte @sbyte) => Write((byte)@sbyte);

        /// <summary>
        /// Write 1 byte(nullable flag) and 1 byte(@sbyte) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @sbyte not null {</para>
        /// <para>1 byte: data (byte)@sbyte</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="sbyte">data sbyte</param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing, or the stream is already closed.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(sbyte? @sbyte)
        {
            if (@sbyte is null) Write(false);
            else
            {
                Write(true);
                Write((byte)@sbyte);
            }
        }

        /// <summary>
        /// Write 1 byte: data (byte)bool
        /// <para>Signature</para>
        /// <para>1 byte: data (byte)bool </para>
        /// </summary>
        /// <param name="bool">data bool</param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing, or the stream is already closed.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(bool @bool) => Write((byte)(@bool ? 1 : 0));

        /// <summary>
        /// Write 1 byte(nullable flag) and 1 byte(@bool) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @bool not null {</para>
        /// <para>1 byte: data (byte)@bool</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="bool">data bool?</param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing, or the stream is already closed.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(bool? @bool)
        {
            if (@bool is null) Write(false);
            else
            {
                Write(true);
                Write((bool)@bool);
            }
        }

        /// <summary>
        /// Write two bytes of short
        /// <para>Signature</para>
        /// <para>2 bytes: data short </para>
        /// </summary>
        /// <param name="short">data short</param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe void Write(short @short)
        {
            var arr = new byte[ShortSize];
            fixed (byte* ptr = arr) *(short*)ptr = @short;
            _wrappedStream.Write(arr, 0, ShortSize);
        }

        /// <summary>
        /// Write 1 byte(nullable flag) and 2 byte(@short) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @short not null {</para>
        /// <para>2 bytes: data short</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="short">data short?</param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing, or the stream is already closed.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(short? @short)
        {
            if (@short is null) Write(false);
            else
            {
                Write(true);
                Write((short)@short);
            }
        }

        /// <summary>
        /// Write two bytes of ushort
        /// <para>Signature</para>
        /// <para>2 bytes: data ushort </para>
        /// </summary>
        /// <param name="ushort">data short</param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe void Write(ushort @ushort)
        {
            var arr = new byte[UShortSize];
            fixed (byte* ptr = arr) *(ushort*)ptr = @ushort;
            _wrappedStream.Write(arr, 0, UShortSize);
        }

        /// <summary>
        /// Write 1 byte(nullable flag) and 2 byte(@ushort) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @ushort not null {</para>
        /// <para>2 bytes: data ushort</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="ushort">data ushort?</param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing, or the stream is already closed.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(ushort? @ushort)
        {
            if (@ushort is null) Write(false);
            else
            {
                Write(true);
                Write((ushort)@ushort);
            }
        }

        /// <summary>
        /// Write two bytes of char
        /// <para>Signature</para>
        /// <para>2 bytes: data char </para>
        /// </summary>
        /// <param name="char">data short</param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe void Write(char @char)
        {
            var arr = new byte[CharSize];
            fixed (byte* ptr = arr) *(char*)ptr = @char;
            _wrappedStream.Write(arr, 0, CharSize);
        }

        /// <summary>
        /// Write 1 byte(nullable flag) and 2 byte(@char) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @char not null {</para>
        /// <para>2 bytes: data char</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="char">data char?</param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing, or the stream is already closed.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(char? @char)
        {
            if (@char is null) Write(false);
            else
            {
                Write(true);
                Write((char)@char);
            }
        }

        /// <summary>
        /// Write 4 bytes of int
        /// <para>Signature</para>
        /// <para>4 bytes: data int </para>
        /// </summary>
        /// <param name="int">data int</param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe void Write(int @int)
        {
            var arr = new byte[IntSize];
            fixed (byte* ptr = arr) *(int*)ptr = @int;
            _wrappedStream.Write(arr, 0, IntSize);
        }

        /// <summary>
        /// Write 1 byte(nullable flag) and 4 byte(@int) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @int not null {</para>
        /// <para>4 bytes: data int</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="int">data int?</param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing, or the stream is already closed.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(int? @int)
        {
            if (@int is null) Write(false);
            else
            {
                Write(true);
                Write((int)@int);
            }
        }

        /// <summary>
        /// Write 4 bytes of uint
        /// <para>Signature</para>
        /// <para>4 bytes: data uint</para>
        /// </summary>
        /// <param name="uint">data uint</param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe void Write(uint @uint)
        {
            var arr = new byte[UIntSize];
            fixed (byte* ptr = arr) *(uint*)ptr = @uint;
            _wrappedStream.Write(arr, 0, UIntSize);
        }

        /// <summary>
        /// Write 1 byte(nullable flag) and 4 byte(@uint) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @uint not null {</para>
        /// <para>4 bytes: data uint</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="uint">data uint?</param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing, or the stream is already closed.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(uint? @uint)
        {
            if (@uint is null) Write(false);
            else
            {
                Write(true);
                Write((uint)@uint);
            }
        }

        /// <summary>
        /// Write 4 bytes of float
        /// <para>Signature</para>
        /// <para>4 bytes: data float</para>
        /// </summary>
        /// <param name="float">data float</param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe void Write(float @float)
        {
            var arr = new byte[FloatSize];
            fixed (byte* ptr = arr) *(float*)ptr = @float;
            _wrappedStream.Write(arr, 0, FloatSize);
        }

        /// <summary>
        /// Write 1 byte(nullable flag) and 4 byte(@float) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @float not null {</para>
        /// <para>4 bytes: data float</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="float">data float?</param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing, or the stream is already closed.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(float? @float)
        {
            if (@float is null) Write(false);
            else
            {
                Write(true);
                Write((float)@float);
            }
        }

        /// <summary>
        /// Write 8 bytes of long
        /// <para>Signature</para>
        /// <para>8 bytes: data long</para>
        /// </summary>
        /// <param name="long">data long</param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe void Write(long @long)
        {
            var arr = new byte[LongSize];
            fixed (byte* ptr = arr) *(long*)ptr = @long;
            _wrappedStream.Write(arr, 0, LongSize);
        }

        /// <summary>
        /// Write 1 byte(nullable flag) and 8 byte(@long) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @long not null {</para>
        /// <para>8 bytes: data long</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="long">data long?</param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing, or the stream is already closed.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(long? @long)
        {
            if (@long is null) Write(false);
            else
            {
                Write(true);
                Write((long)@long);
            }
        }

        /// <summary>
        /// Write 8 bytes of ulong
        /// <para>Signature</para>
        /// <para>8 bytes: data ulong</para>
        /// </summary>
        /// <param name="ulong">data ulong</param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe void Write(ulong @ulong)
        {
            var arr = new byte[ULongSize];
            fixed (byte* ptr = arr) *(ulong*)ptr = @ulong;
            _wrappedStream.Write(arr, 0, ULongSize);
        }

        /// <summary>
        /// Write 1 byte(nullable flag) and 8 byte(@ulong) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @ulong not null {</para>
        /// <para>8 bytes: data ulong</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="ulong">data ulong?</param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing, or the stream is already closed.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(ulong? @ulong)
        {
            if (@ulong is null) Write(false);
            else
            {
                Write(true);
                Write((ulong)@ulong);
            }
        }

        /// <summary>
        /// Write 8 bytes of double
        /// <para>Signature</para>
        /// <para>8 bytes: data double</para>
        /// </summary>
        /// <param name="double">data double</param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe void Write(double @double)
        {
            var arr = new byte[DoubleSize];
            fixed (byte* ptr = arr) *(double*)ptr = @double;
            _wrappedStream.Write(arr, 0, DoubleSize);
        }

        /// <summary>
        /// Write 1 byte(nullable flag) and 8 byte(@double) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @double not null {</para>
        /// <para>8 bytes: data double</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="double">data double?</param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing, or the stream is already closed.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(double? @double)
        {
            if (@double is null) Write(false);
            else
            {
                Write(true);
                Write((double)@double);
            }
        }

        /// <summary>
        /// Write 8 bytes of DateTime
        /// <para>Signature</para>
        /// <para>8 bytes: data dateTime</para>
        /// </summary>
        /// <param name="dateTime">data dateTime</param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(DateTime dateTime) => Write(dateTime.ToBinary());

        /// <summary>
        /// Write 1 byte(nullable flag) and 8 byte(dateTime as long) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @double not null {</para>
        /// <para>8 bytes: data (long)dateTime</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="dateTime">data dateTime?</param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing, or the stream is already closed.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(DateTime? dateTime)
        {
            if (dateTime is null) Write(false);
            else
            {
                Write(true);
                Write((DateTime)dateTime);
            }
        }

        /// <summary>
        /// Write 8 bytes of TimeSpan.Ticks
        /// <para>Signature</para>
        /// <para>8 bytes: data timeSpan</para>
        /// </summary>
        /// <param name="timeSpan">data timeSpan</param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(TimeSpan timeSpan) => Write(timeSpan.Ticks);

        /// <summary>
        /// Write 1 byte(nullable flag) and 8 byte(timeSpan.Ticks) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @double not null {</para>
        /// <para>8 bytes: data (long)timeSpan.Ticks</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="timeSpan">data dateTime?</param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing, or the stream is already closed.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(TimeSpan? timeSpan)
        {
            if (timeSpan is null) Write(false);
            else
            {
                Write(true);
                Write((TimeSpan)timeSpan);
            }
        }

        /// <summary>
        /// Write 16 bytes of Guid
        /// <para>Signature</para>
        /// <para>16 bytes: data guid</para>
        /// </summary>
        /// <param name="guid">data guid</param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe void Write(Guid guid)
        {
            var arr = new byte[GuidSize];
            fixed (byte* ptr = arr) *(Guid*)ptr = guid;
            _wrappedStream.Write(arr, 0, GuidSize);
        }

        /// <summary>
        /// Write 1 byte(nullable flag) and 16 byte(Guid) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @double not null {</para>
        /// <para>16 bytes: data guid</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="guid">data Guid?</param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing, or the stream is already closed.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(Guid? guid)
        {
            if (guid is null) Write(false);
            else
            {
                Write(true);
                Write((Guid)guid);
            }
        }

        /// <summary>
        /// Write 16 bytes of decimal
        /// <para>Signature</para>
        /// <para>4 bytes: data decimal[0]</para>
        /// <para>4 bytes: data decimal[1]</para>
        /// <para>4 bytes: data decimal[2]</para>
        /// <para>4 bytes: data decimal[3]</para>
        /// </summary>
        /// <param name="decimal">data decimal</param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(decimal @decimal)
        {
            var bits = decimal.GetBits(@decimal);
            Write(bits[0]);
            Write(bits[1]);
            Write(bits[2]);
            Write(bits[3]);
        }

        /// <summary>
        /// Write 1 byte(nullable flag) and 16 byte(decimal) if not null
        /// <para>Signature</para>
        /// <para>1 byte: 0x00 | 0x01</para>
        /// <para>if @decimal not null {</para>
        /// <para>4 bytes: data decimal[0]</para>
        /// <para>4 bytes: data decimal[1]</para>
        /// <para>4 bytes: data decimal[2]</para>
        /// <para>4 bytes: data decimal[3]</para>
        /// <para>}</para>
        /// </summary>
        /// <param name="decimal">data decimal?</param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing, or the stream is already closed.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(decimal? @decimal)
        {
            if (@decimal is null) Write(false);
            else
            {
                Write(true);
                Write((decimal)@decimal);
            }
        }

        /// <summary>
        /// Write string by char and write end of string symbol ('\0')
        /// </summary>
        /// <param name="string"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(string @string)
        {
            foreach (var symbol in @string) Write(symbol);
            Write('\0');
        }

        /// <summary>
        /// Write string byte array length and byte array by chosen encoding
        /// </summary>
        /// <param name="string">data string</param>
        /// <param name="encoding">chosen encoding</param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(string @string, Encoding encoding)
        {
            var byteString = encoding.GetBytes(@string);
            Write((ushort)byteString.Length); // write size
            _wrappedStream.Write(byteString);
        }

        /// <summary>
        /// Write 2 byte of IList.Count as ushort and write byte array
        /// </summary>
        /// <param name="byteArray"></param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        /// <exception cref="ApplicationException"></exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(byte[] byteArray)
        {
            var count = byteArray.Length;
            ContractRules.CantBeMoreThen(count, ushort.MaxValue, $"Cannot write more items then {ushort.MaxValue}");
            Write((ushort)count);
            _wrappedStream.Write(byteArray, 0, count);
        }

        /// <summary>
        /// Write 2 byte of IList.Count as ushort and write list of byte
        /// </summary>
        /// <param name="list"></param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        /// <exception cref="ApplicationException"></exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(IList<byte> list)
        {
            var count = list.Count;
            ContractRules.CantBeMoreThen(count, ushort.MaxValue, $"Cannot write more items then {ushort.MaxValue}");
            Write((ushort)count);
            foreach (var current in list) Write(current);
        }

        /// <summary>
        /// Write 2 byte of IList.Count as ushort and write list of sbyte
        /// </summary>
        /// <param name="list"></param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        /// <exception cref="ApplicationException"></exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(IList<sbyte> list)
        {
            var count = list.Count;
            count.CantBeMoreThen(ushort.MaxValue, $"Cannot write more items then {ushort.MaxValue}");
            Write((ushort)count);
            foreach (var current in list) Write(current);
        }

        /// <summary>
        /// Write 2 byte of IList.Count as ushort and write list of bool
        /// </summary>
        /// <param name="list"></param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        /// <exception cref="ApplicationException"></exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(IList<bool> list)
        {
            var count = list.Count;
            count.CantBeMoreThen(ushort.MaxValue, $"Cannot write more items then {ushort.MaxValue}");
            Write((ushort)count);
            foreach (var current in list) Write(current);
        }

        /// <summary>
        /// Write 2 byte of IList.Count as ushort and write list of short
        /// </summary>
        /// <param name="list"></param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        /// <exception cref="ApplicationException"></exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(IList<short> list)
        {
            var count = list.Count;
            count.CantBeMoreThen(ushort.MaxValue, $"Cannot write more items then {ushort.MaxValue}");
            Write((ushort)count);
            foreach (var current in list) Write(current);
        }

        /// <summary>
        /// Write 2 byte of IList.Count as ushort and write list of ushort
        /// </summary>
        /// <param name="list"></param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        /// <exception cref="ApplicationException"></exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(IList<ushort> list)
        {
            var count = list.Count;
            count.CantBeMoreThen(ushort.MaxValue, $"Cannot write more items then {ushort.MaxValue}");
            Write((ushort)count);
            foreach (var current in list) Write(current);
        }

        /// <summary>
        /// Write 2 byte of IList.Count as ushort and write list of char
        /// </summary>
        /// <param name="list"></param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        /// <exception cref="ApplicationException"></exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(IList<char> list)
        {
            var count = list.Count;
            count.CantBeMoreThen(ushort.MaxValue, $"Cannot write more items then {ushort.MaxValue}");
            Write((ushort)count);
            foreach (var current in list) Write(current);
        }

        /// <summary>
        /// Write 2 byte of IList.Count as ushort and write list of int
        /// </summary>
        /// <param name="list"></param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        /// <exception cref="ApplicationException"></exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(IList<int> list)
        {
            var count = list.Count;
            count.CantBeMoreThen(ushort.MaxValue, $"Cannot write more items then {ushort.MaxValue}");
            Write((ushort)count);
            foreach (var current in list) Write(current);
        }

        /// <summary>
        /// Write 2 byte of IList.Count as ushort and write list of uint
        /// </summary>
        /// <param name="list"></param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        /// <exception cref="ApplicationException"></exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(IList<uint> list)
        {
            var count = list.Count;
            count.CantBeMoreThen(ushort.MaxValue, $"Cannot write more items then {ushort.MaxValue}");
            Write((ushort)count);
            foreach (var current in list) Write(current);
        }

        /// <summary>
        /// Write 2 byte of IList.Count as ushort and write list of float
        /// </summary>
        /// <param name="list"></param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        /// <exception cref="ApplicationException"></exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(IList<float> list)
        {
            var count = list.Count;
            count.CantBeMoreThen(ushort.MaxValue, $"Cannot write more items then {ushort.MaxValue}");
            Write((ushort)count);
            foreach (var current in list) Write(current);
        }

        /// <summary>
        /// Write 2 byte of IList.Count as ushort and write list of long
        /// </summary>
        /// <param name="list"></param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        /// <exception cref="ApplicationException"></exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(IList<long> list)
        {
            var count = list.Count;
            count.CantBeMoreThen(ushort.MaxValue, $"Cannot write more items then {ushort.MaxValue}");
            Write((ushort)count);
            foreach (var current in list) Write(current);
        }

        /// <summary>
        /// Write 2 byte of IList.Count as ushort and write list of ulong
        /// </summary>
        /// <param name="list"></param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        /// <exception cref="ApplicationException"></exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(IList<ulong> list)
        {
            var count = list.Count;
            count.CantBeMoreThen(ushort.MaxValue, $"Cannot write more items then {ushort.MaxValue}");
            Write((ushort)count);
            foreach (var current in list) Write(current);
        }

        /// <summary>
        /// Write 2 byte of IList.Count as ushort and write list of double
        /// </summary>
        /// <param name="list"></param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        /// <exception cref="ApplicationException"></exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(IList<double> list)
        {
            var count = list.Count;
            count.CantBeMoreThen(ushort.MaxValue, $"Cannot write more items then {ushort.MaxValue}");
            Write((ushort)count);
            foreach (var current in list) Write(current);
        }

        /// <summary>
        /// Write 2 byte of IList.Count as ushort and write list of DateTime
        /// </summary>
        /// <param name="list"></param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        /// <exception cref="ApplicationException"></exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(IList<DateTime> list)
        {
            var count = list.Count;
            count.CantBeMoreThen(ushort.MaxValue, $"Cannot write more items then {ushort.MaxValue}");
            Write((ushort)count);
            foreach (var current in list) Write(current);
        }

        /// <summary>
        /// Write 2 byte of IList.Count as ushort and write list of TimeSpan
        /// </summary>
        /// <param name="list"></param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        /// <exception cref="ApplicationException"></exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(IList<TimeSpan> list)
        {
            var count = list.Count;
            count.CantBeMoreThen(ushort.MaxValue, $"Cannot write more items then {ushort.MaxValue}");
            Write((ushort)count);
            foreach (var current in list) Write(current);
        }

        /// <summary>
        /// Write 2 byte of IList.Count as ushort and write list of Guid
        /// </summary>
        /// <param name="list"></param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        /// <exception cref="ApplicationException"></exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(IList<Guid> list)
        {
            var count = list.Count;
            count.CantBeMoreThen(ushort.MaxValue, $"Cannot write more items then {ushort.MaxValue}");
            Write((ushort)count);
            foreach (var current in list) Write(current);
        }

        /// <summary>
        /// Write 2 byte of IList.Count as ushort and write list of decimal
        /// </summary>
        /// <param name="list"></param>
        /// <exception cref="T:System.ArgumentException">Wrapped stream size too small to write.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occured, such as the specified file cannot be found.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
        /// <exception cref="T:System.ObjectDisposedException"><see cref="System.IO.Stream.Write(byte[],int,int)"></see> was called after the stream was closed.</exception>
        /// <exception cref="ApplicationException"></exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(IList<decimal> list)
        {
            var count = list.Count;
            count.CantBeMoreThen(ushort.MaxValue, $"Cannot write more items then {ushort.MaxValue}");
            Write((ushort)count);
            foreach (var current in list) Write(current);
        }

        /*
        /// <summary>
        /// Writes 2 byte of IList.Count as ushort and write list of known struct by type
        /// </summary>
        /// <typeparam name="T">type of IList</typeparam>
        /// <param name="list">list of known structs</param>
        [Obsolete("This method too slow. Better use overloaded methods")]
        [SuppressMessage("ReSharper", "PossibleInvalidOperationException")]
        public void Write<T>(IList<T> list) where T : struct
        {
            var count = list.Count;
            if (count > ushort.MaxValue) throw new ApplicationException($"Cannot write more objects then {ushort.MaxValue}");
            Write((ushort)count);
            switch (default(T))
            {
                case byte _: foreach (var current in list) Write((byte)(current as byte?)); break;
                case sbyte _: foreach (var current in list) Write((sbyte)(current as sbyte?)); break;
                case bool _: foreach (var current in list) Write((bool)(current as bool?)); break;
                case short _: foreach (var current in list) Write((short)(current as short?)); break;
                case ushort _: foreach (var current in list) Write((ushort)(current as ushort?)); break;
                case char _: foreach (var current in list) Write((char)(current as char?)); break;
                case int _: foreach (var current in list) Write((int)(current as int?)); break;
                case uint _: foreach (var current in list) Write((uint)(current as uint?)); break;
                case float _: foreach (var current in list) Write((float)(current as float?)); break;
                case long _: foreach (var current in list) Write((long)(current as long?)); break;
                case ulong _: foreach (var current in list) Write((ulong)(current as ulong?)); break;
                case double _: foreach (var current in list) Write((double)(current as double?)); break;
                case DateTime _: foreach (var current in list) Write((DateTime)(current as DateTime?)); break;
                case TimeSpan _: foreach (var current in list) Write((TimeSpan)(current as TimeSpan?)); break;
                case Guid _: foreach (var current in list) Write((Guid)(current as Guid?)); break;
                case decimal _: foreach (var current in list) Write((decimal)(current as decimal?)); break;
                default: throw new ApplicationException($"Cannot write list of unknown struct {typeof(T)}");
            }
        }*/


        #endregion

        #region Read

        /// <summary>
        /// Reads 1 byte from stream.
        /// </summary>
        /// <returns>Read byte</returns>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        /// <exception cref="T:System.IO.EndOfStreamException">Reach end of stream.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public byte ReadByte()
        {
            var b = _wrappedStream.ReadByte();
            if (b == -1) throw new EndOfStreamException();
            return (byte)b;
        }

        /// <summary>
        /// Read 1 byte of nullable and 1 data byte from stream
        /// </summary>
        /// <returns>Read byte or null</returns>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        /// <exception cref="T:System.IO.EndOfStreamException">Reach end of stream.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public byte? ReadByteNullable() => ReadBool() ? ReadByte() : (byte?)null;


        /// <summary>
        /// Read 1 byte as sbyte from stream.
        /// </summary>
        /// <returns>Read sbyte</returns>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        /// <exception cref="T:System.IO.EndOfStreamException">Reach end of stream.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public sbyte ReadSByte() => (sbyte)ReadByte();

        /// <summary>
        /// Read 1 byte of nullable and 1 data byte as sbyte from stream
        /// </summary>
        /// <returns>Read sbyte or null</returns>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        /// <exception cref="T:System.IO.EndOfStreamException">Reach end of stream.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public sbyte? ReadSByteNullable() => ReadBool() ? ReadSByte() : (sbyte?)null;


        /// <summary>
        /// Read 1 byte as bool from stream.
        /// </summary>
        /// <returns>Read bool</returns>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        /// <exception cref="T:System.IO.EndOfStreamException">Reach end of stream.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool ReadBool() => ReadByte() == 1;

        /// <summary>
        /// Read 1 byte of nullable and 1 data byte as bool from stream
        /// </summary>
        /// <returns>Read bool or null</returns>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        /// <exception cref="T:System.IO.EndOfStreamException">Reach end of stream.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool? ReadBoolNullable() => ReadBool()? ReadBool() : (bool?)null;

        /// <summary>
        /// Read 2 bytes as short from stream
        /// </summary>
        /// <returns>Read short</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe short ReadShort()
        {
            var arr = new byte[ShortSize];
            _wrappedStream.Read(arr, 0, ShortSize);
            fixed (byte* ptr = &arr[0]) return *(short*)ptr;
        }

        /// <summary>
        /// Read 1 byte of nullable and 2 data bytes as short from stream
        /// </summary>
        /// <returns>Read short or null</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public short? ReadShortNullable() => ReadBool() ? ReadShort() : (short?)null;

        /// <summary>
        /// Read 2 bytes as ushort from stream
        /// </summary>
        /// <returns>Read ushort</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe ushort ReadUShort()
        {
            var arr = new byte[UShortSize];
            _wrappedStream.Read(arr, 0, UShortSize);
            fixed (byte* ptr = &arr[0]) return *(ushort*)ptr;
        }

        /// <summary>
        /// Read 1 byte of nullable and 2 data bytes as ushort from stream
        /// </summary>
        /// <returns>Read ushort or null</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ushort? ReadUShortNullable() => ReadBool() ? ReadUShort(): (ushort?)null;

        /// <summary>
        /// Read 2 bytes as char from stream
        /// </summary>
        /// <returns>Read char</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe char ReadChar()
        {
            var arr = new byte[CharSize];
            _wrappedStream.Read(arr, 0, CharSize);
            fixed (byte* ptr = &arr[0]) return *(char*)ptr;
        }

        /// <summary>
        /// Read 1 byte of nullable and 2 data bytes as char from stream
        /// </summary>
        /// <returns>Read char or null</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public char? ReadCharNullable() => ReadBool() ? ReadChar() : (char?)null;

        /// <summary>
        /// Read 4 bytes as int from stream
        /// </summary>
        /// <returns>Read int</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe int ReadInt()
        {
            var arr = new byte[IntSize];
            _wrappedStream.Read(arr, 0, IntSize);
            fixed (byte* ptr = &arr[0]) return *(int*)ptr;
        }

        /// <summary>
        /// Read 1 byte of nullable and 4 data bytes as int from stream
        /// </summary>
        /// <returns>Read int or null</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int? ReadIntNullable() => ReadBool() ? ReadInt() : (int?)null;

        /// <summary>
        /// Read 4 bytes as uint from stream
        /// </summary>
        /// <returns>Read uint</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe uint ReadUInt()
        {
            var arr = new byte[UIntSize];
            _wrappedStream.Read(arr, 0, UIntSize);
            fixed (byte* ptr = &arr[0]) return *(uint*)ptr;
        }

        /// <summary>
        /// Read 1 byte of nullable and 4 data bytes as uint from stream
        /// </summary>
        /// <returns>Read uint or null</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public uint? ReadUIntNullable() => ReadBool() ? ReadUInt() : (uint?)null;


        /// <summary>
        /// Read 4 bytes as float from stream
        /// </summary>
        /// <returns>Read float</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe float ReadFloat()
        {
            var arr = new byte[FloatSize];
            _wrappedStream.Read(arr, 0, FloatSize);
            fixed (byte* ptr = &arr[0]) return *(float*)ptr;
        }

        /// <summary>
        /// Read 1 byte of nullable and 4 data bytes as float from stream
        /// </summary>
        /// <returns>Read float or null</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public float? ReadFloatNullable() => ReadBool() ? ReadFloat() : (float?)null;

        /// <summary>
        /// Read 8 bytes as long from stream
        /// </summary>
        /// <returns>Read long</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe long ReadLong()
        {
            var arr = new byte[LongSize];
            _wrappedStream.Read(arr, 0, LongSize);
            fixed (byte* ptr = &arr[0]) return *(long*)ptr;
        }

        /// <summary>
        /// Read 1 byte of nullable and 8 data bytes as long from stream
        /// </summary>
        /// <returns>Read long or null</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public long? ReadLongNullable() => ReadBool() ? ReadLong() : (long?)null;

        /// <summary>
        /// Read 8 bytes as ulong from stream
        /// </summary>
        /// <returns>Read ulong</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe ulong ReadULong()
        {
            var arr = new byte[ULongSize];
            _wrappedStream.Read(arr, 0, ULongSize);
            fixed (byte* ptr = &arr[0]) return *(ulong*)ptr;
        }

        /// <summary>
        /// Read 1 byte of nullable and 8 data bytes as long from stream
        /// </summary>
        /// <returns>Read ulong or null</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ulong? ReadULongNullable() => ReadBool() ? ReadULong() : (ulong?)null;

        /// <summary>
        /// Read 8 bytes as double from stream
        /// </summary>
        /// <returns>Read double</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe double ReadDouble()
        {
            var arr = new byte[DoubleSize];
            _wrappedStream.Read(arr, 0, DoubleSize);
            fixed (byte* ptr = &arr[0]) return *(double*)ptr;
        }

        /// <summary>
        /// Read 1 byte of nullable and 8 data bytes as double from stream
        /// </summary>
        /// <returns>Read double or null</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public double? ReadDoubleNullable() => ReadBool() ? ReadDouble() : (double?)null;

        /// <summary>
        /// Read 8 bytes as DateTime from stream
        /// </summary>
        /// <returns>Read DateTime</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public DateTime ReadDateTime() => DateTime.FromBinary(ReadLong());

        /// <summary>
        /// Read 1 byte of nullable and 8 data bytes as DateTime from stream
        /// </summary>
        /// <returns>Read DateTime or null</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public DateTime? ReadDateTimeNullable() => ReadBool() ? ReadDateTime() : (DateTime?)null;

        /// <summary>
        /// Read 8 bytes as TimeSpan from stream
        /// </summary>
        /// <returns>Read TimeSpan</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        public TimeSpan ReadTimeSpan() => TimeSpan.FromTicks(ReadLong());

        /// <summary>
        /// Read 1 byte of nullable and 8 data bytes as TimeSpan from stream
        /// </summary>
        /// <returns>Read DateTime or null</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public TimeSpan? ReadTimeSpanNullable() => ReadBool() ? ReadTimeSpan() : (TimeSpan?)null;

        /// <summary>
        /// Read 16 bytes as Guid from stream
        /// </summary>
        /// <returns>Read Guid</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Guid ReadGuid()
        {
            var result = new byte[GuidSize];
            _wrappedStream.Read(result, 0, GuidSize);
            return new Guid(result);
        }

        /// <summary>
        /// Read 1 byte of nullable and 16 data bytes as Guid from stream
        /// </summary>
        /// <returns>Read Guid or null</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Guid? ReadGuidNullable() => ReadBool() ? ReadGuid() : (Guid?)null;

        /// <summary>
        /// Read 16 bytes as decimal from stream
        /// </summary>
        /// <returns>Read decimal</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public decimal ReadDecimal()
        {
            var bits = new[] { ReadInt(), ReadInt(), ReadInt(), ReadInt() };
            var sign = (bits[3] & 0x80000000) != 0;
            var scale = (byte)((bits[3] >> 16) & 0x7F);
            return new decimal(bits[0], bits[1], bits[2], sign, scale);
        }

        /// <summary>
        /// Read 1 byte of nullable and 16 data bytes as decimal from stream
        /// </summary>
        /// <returns>Read decimal or null</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public decimal? ReadDecimalNullable() => ReadBool() ? ReadDecimal() : (decimal?)null;

        /// <summary>
        /// Read string until end of string symbol
        /// </summary>
        /// <returns>Read string</returns>
        public string ReadString()
        {
            var pString = new StringBuilder();
            char ch;
            while ((ch = ReadChar()) != '\0') pString.Append(ch);
            return pString.ToString();
        }

        /// <summary>
        /// Read 2 bytes of size as ushort and read string by chosen encoding
        /// </summary>
        /// <param name="encoding"></param>
        /// <returns>Read string in chosen encoding</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        public string ReadString(Encoding encoding) => encoding.GetString(ReadByteArray());

        /// <summary>
        /// Read 2 byte of size as ushort and n bytes 
        /// </summary>
        /// <returns>Read byte array</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public byte[] ReadByteArray()
        {
            var result = new byte[ReadUShort()];
            _wrappedStream.Read(result, 0, result.Length);
            return result;
        }

        /// <summary>
        /// Read 2 byte of size as ushort and n bytes as sbytes
        /// </summary>
        /// <returns>Read sbyte array</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public sbyte[] ReadSByteArray()
        {
            var result = new sbyte[ReadUShort()];
            for (var i = 0; i < result.Length; i++) result[i] = ReadSByte();
            return result;
        }

        /// <summary>
        /// Read 2 byte of size as ushort and n bytes as bool
        /// </summary>
        /// <returns>Read sbyte array</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool[] ReadBoolArray()
        {
            var result = new bool[ReadUShort()];
            for (var i = 0; i < result.Length; i++) result[i] = ReadBool();
            return result;
        }

        /// <summary>
        /// Read 2 byte of size as ushort and n bytes as short
        /// </summary>
        /// <returns>Read short array</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public short[] ReadShortArray()
        {
            var result = new short[ReadUShort()];
            for (var i = 0; i < result.Length; i++) result[i] = ReadShort();
            return result;
        }

        /// <summary>
        /// Read 2 byte of size as ushort and n bytes as ushort
        /// </summary>
        /// <returns>Read ushort array</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ushort[] ReadUShortArray()
        {
            var result = new ushort[ReadUShort()];
            for (var i = 0; i < result.Length; i++) result[i] = ReadUShort();
            return result;
        }

        /// <summary>
        /// Read 2 byte of size as ushort and n bytes as char
        /// </summary>
        /// <returns>Read char array</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ushort[] ReadCharArray()
        {
            var result = new ushort[ReadUShort()];
            for (var i = 0; i < result.Length; i++) result[i] = ReadChar();
            return result;
        }

        /// <summary>
        /// Read 2 byte of size as ushort and n bytes as int
        /// </summary>
        /// <returns>Read int array</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int[] ReadIntArray()
        {
            var result = new int[ReadUShort()];
            for (var i = 0; i < result.Length; i++) result[i] = ReadInt();
            return result;
        }

        /// <summary>
        /// Read 2 byte of size as ushort and n bytes as uint
        /// </summary>
        /// <returns>Read uint array</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public uint[] ReadUIntArray()
        {
            var result = new uint[ReadUShort()];
            for (var i = 0; i < result.Length; i++) result[i] = ReadUInt();
            return result;
        }

        /// <summary>
        /// Read 2 byte of size as ushort and n bytes as float
        /// </summary>
        /// <returns>Read floats array</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public float[] ReadFloatArray()
        {
            var result = new float[ReadUShort()];
            for (var i = 0; i < result.Length; i++) result[i] = ReadFloat();
            return result;
        }

        /// <summary>
        /// Read 2 byte of size as ushort and n bytes as long 
        /// </summary>
        /// <returns>Read long array</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public long[] ReadLongArray()
        {
            var result = new long[ReadUShort()];
            for (var i = 0; i < result.Length; i++) result[i] = ReadLong();
            return result;
        }

        /// <summary>
        /// Read 2 byte of size as ushort and n bytes as ulong 
        /// </summary>
        /// <returns>Read ulong array</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ulong[] ReadULongArray()
        {
            var result = new ulong[ReadUShort()];
            for (var i = 0; i < result.Length; i++) result[i] = ReadULong();
            return result;
        }

        /// <summary>
        /// Read 2 byte of size as ushort and n bytes as ulong 
        /// </summary>
        /// <returns>Read double array</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public double[] ReadDoubleArray()
        {
            var result = new double[ReadUShort()];
            for (var i = 0; i < result.Length; i++) result[i] = ReadDouble();
            return result;
        }

        /// <summary>
        /// Read 2 byte of size as ushort and n bytes as DateTime 
        /// </summary>
        /// <returns>Read DateTime array</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public DateTime[] ReadDateTimeArray()
        {
            var result = new DateTime[ReadUShort()];
            for (var i = 0; i < result.Length; i++) result[i] = ReadDateTime();
            return result;
        }

        /// <summary>
        /// Read 2 byte of size as ushort and n bytes as TimeSpan 
        /// </summary>
        /// <returns>Read TimeSpan array</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public TimeSpan[] ReadTimeSpanArray()
        {
            var result = new TimeSpan[ReadUShort()];
            for (var i = 0; i < result.Length; i++) result[i] = ReadTimeSpan();
            return result;
        }

        /// <summary>
        /// Read 2 byte of size as ushort and n bytes as Guid 
        /// </summary>
        /// <returns>Read Guid array</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Guid[] ReadGuidArray()
        {
            var result = new Guid[ReadUShort()];
            for (var i = 0; i < result.Length; i++) result[i] = ReadGuid();
            return result;
        }

        /// <summary>
        /// Read 2 byte of size as ushort and n bytes as decimal 
        /// </summary>
        /// <returns>Read decimal array</returns>
        /// <exception cref="T:System.ArgumentException">Out of range.</exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
        /// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public decimal[] ReadDecimalArray()
        {
            var result = new decimal[ReadUShort()];
            for (var i = 0; i < result.Length; i++) result[i] = ReadDecimal();
            return result;
        }
        #endregion

        #endregion

        #endregion

        #region protected (Extension class data/logic API)

        #region data

        #endregion

        #region actions

        #endregion

        #endregion

        #region internal (Core-lib class data/logic API ❢❢❢ this is only my ❢❢❢) 

        #region data

        #endregion

        #region actions

        #endregion

        #endregion

        #region private (Core-class data/logic API ❢❢❢ this is only my ❢❢❢)

        #region data
        private const int ByteSize = sizeof(byte);
        private const int ShortSize = sizeof(short);
        private const int UShortSize = sizeof(ushort);
        private const int CharSize = sizeof(char);
        private const int IntSize = sizeof(int);
        private const int UIntSize = sizeof(uint);
        private const int FloatSize = sizeof(float);
        private const int LongSize = sizeof(long);
        private const int ULongSize = sizeof(ulong);
        private const int DoubleSize = sizeof(double);
        private static readonly unsafe int GuidSize = sizeof(Guid);
        private readonly System.IO.Stream _wrappedStream;

        #endregion

        #region actions

        #endregion

        #endregion
    }
}