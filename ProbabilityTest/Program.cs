﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using JilCore.Core.Utils.Random;
using Microsoft.VisualBasic;

namespace ProbabilityTest
{
    class Program
    {
        public const int chance = 70;
        public const int looseStreakMax = 3;

        static void Main(string[] args)
        {
            var counterBadStreak = 0;

            var goodPuhas = new List<Pushka>();
            for (var i = 0; i < 10; i++) goodPuhas.Add(new Pushka(true));
            var badPuhas = new List<Pushka>();
            for (var i = 0; i < 1000; i++) badPuhas.Add(new Pushka(false));


            for (int i = 0; i < 1000; i++)
            {
                DoMainLogic(goodPuhas, badPuhas, ref counterBadStreak);
                Console.WriteLine($"Хороших пух {goodPuhas.Where(a => !a.IsDeleted).ToArray().Length}");
                Console.WriteLine($"Плохих пух {badPuhas.Where(a => !a.IsDeleted).ToArray().Length}");
            }

            foreach (var goodPuha in goodPuhas.Where(a => !a.IsDeleted))
            {
                Console.WriteLine($"Хорошая пуха на +{goodPuha.Enchant}");
            }
        }

        public static void DoMainLogic(List<Pushka> goodPuhas, List<Pushka> badPuhas, ref int counterBadStreak)
        {
            foreach (var badPuha in badPuhas.Where(a => !a.IsDeleted))
            {
                if (DoEnchant(badPuha))
                {
                    Console.WriteLine($"Плохая пуха затачилась на {badPuha.Enchant}");
                    counterBadStreak = 0;
                }
                else
                {
                    counterBadStreak++;
                    Console.WriteLine("Плохая пуха сломалась");
                }
                if (counterBadStreak == looseStreakMax)
                {
                    counterBadStreak = 0;
                    foreach (var goodPuha in goodPuhas.Where(a => !a.IsDeleted))
                    {
                        if (DoEnchant(goodPuha))
                        {
                            Console.WriteLine($"Хорошая пуха затачилась на {goodPuha.Enchant}");
                            return;
                        }
                        else
                        {
                            Console.WriteLine("Хорошая пуха сломалась");
                        }
                    }
                }
            }
        }

        public static bool DoEnchant(Pushka puha)
        {
            Thread.Sleep(500);
            if (JilRandomizer.Chance(chance))
            {
                puha.Enchant++;
                return true;
            }
            else
            {
                puha.IsDeleted = true;
                return false;
            }
        }
    }
}
