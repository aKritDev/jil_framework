﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace JilToolkit.Utils.Metric
{
    public static class CodeSpeedChecker
    {
        /// <summary>
        /// memuse / 1024.0 / 1024.0 mb
        /// </summary>
        /// <param name="checkedCode"></param>
        /// <param name="executionTimeAverage"></param>
        /// <param name="usedMemoryAverage"></param>
        public static void RunOnce(Action checkedCode, ref long executionTimeAverage, ref long usedMemoryAverage)
        {
            var sw = new Stopwatch();
            var mem0 = Process.GetCurrentProcess().PeakVirtualMemorySize64;
            sw.Restart();
            checkedCode();
            sw.Stop();
            usedMemoryAverage = Process.GetCurrentProcess().PeakVirtualMemorySize64 - mem0;
            executionTimeAverage = sw.ElapsedNanoSeconds();
            Console.WriteLine($"executionTime: {executionTimeAverage} ns\r\nusedMemory: {usedMemoryAverage} bytes\r\n-----------------------\r\n");
        }

        /// <summary>
        /// memuse / 1024.0 / 1024.0 mb
        /// </summary>
        /// <param name="checkedCode"></param>
        /// <param name="times"></param>
        /// <param name="executionTimeAverage"></param>
        /// <param name="usedMemoryAverage"></param>
        public static void Run(Action checkedCode, int times, ref long executionTimeAverage, ref long usedMemoryAverage)
        {
            var sw = new Stopwatch();
            for (var i = 0; i < times; i++)
            {
                var mem0 = Process.GetCurrentProcess().PeakVirtualMemorySize64;
                sw.Restart();
                checkedCode();
                sw.Stop();
                usedMemoryAverage += Process.GetCurrentProcess().PeakVirtualMemorySize64 - mem0;
                executionTimeAverage += sw.ElapsedNanoSeconds();
            }
            executionTimeAverage /= times;
            usedMemoryAverage /= times;
            Console.WriteLine($"executionTime: {executionTimeAverage} ns\r\nusedMemory: {usedMemoryAverage} bytes\r\n-----------------------\r\n");
        }

        /// <summary>
        /// memuse / 1024.0 / 1024.0 mb
        /// </summary>
        /// <param name="checkedCode"></param>
        /// <param name="times"></param>
        /// <param name="executionTimeAverage"></param>
        /// <param name="usedMemoryAverage"></param>
        public static void RunParallel(Action checkedCode, int times, ref long executionTimeAverage, ref long usedMemoryAverage)
        {
            var usedMemoryAverageTemp = new ConcurrentQueue<long>();
            var executionTimeAverageTemp = new ConcurrentQueue<long>();
            Parallel.For(0, times, a =>
            {
                var mem0 = Process.GetCurrentProcess().PeakVirtualMemorySize64;
                var sw = Stopwatch.StartNew();
                checkedCode();
                sw.Stop();
                usedMemoryAverageTemp.Enqueue(Process.GetCurrentProcess().PeakVirtualMemorySize64 - mem0);
                executionTimeAverageTemp.Enqueue(sw.ElapsedNanoSeconds());
            });
            for (var i = 0; i < times; i++)
            {
                if (usedMemoryAverageTemp.TryDequeue(out var result)) executionTimeAverage += result;
                else throw new ApplicationException();
                if (executionTimeAverageTemp.TryDequeue(out var result2)) usedMemoryAverage += result2;
                else throw new ApplicationException();
            }
            executionTimeAverage /= times;
            usedMemoryAverage /= times;
            Console.WriteLine($"executionTime: {executionTimeAverage} ns\r\nusedMemory: {usedMemoryAverage} bytes\r\n-----------------------\r\n");
        }

        /// <summary>
        ///     Возвращает время выполнения кода в наносекундах.
        /// </summary>
        /// <param name="watch"></param>
        /// <returns></returns>
        public static long ElapsedNanoSeconds(this Stopwatch watch) => watch.ElapsedTicks * 1000000000 / Stopwatch.Frequency;
        /// <summary>
        ///     Возвращает время выполнения кода в микросекундах.
        /// </summary>
        /// <param name="watch"></param>
        /// <returns></returns>
        public static long ElapsedMicroSeconds(this Stopwatch watch) => watch.ElapsedTicks * 1000000 / Stopwatch.Frequency;
    }
}
