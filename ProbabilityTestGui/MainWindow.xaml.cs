﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JilCore.Core.Utils.Random;
using ProbabilityTestGui.Annotations;

namespace ProbabilityTestGui
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public static ObservableCollection<Weapon> GoodsWeapons = new ObservableCollection<Weapon>();
        public static ObservableCollection<Weapon> BadsWeapons = new ObservableCollection<Weapon>();
        private int _looseStrikeCount;
        private int _currentMaxLooseStreak;
        private StringBuilder _log;

        public int LooseStrikeCount
        {
            get => _looseStrikeCount;
            set
            {
                _looseStrikeCount = value;
                OnPropertyChanged();
            }
        }

        public int CurrentMaxLooseStreak
        {
            get => _currentMaxLooseStreak;
            set
            {
                _currentMaxLooseStreak = value;
                OnPropertyChanged();
            }
        }

        public string Log
        {
            get => _log.ToString();
            set
            {
                _log = _log.AppendLine(value);
                OnPropertyChanged();
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            _log = new StringBuilder();
            for (var i = 0; i < 3; i++) GoodsWeapons.Add(new Weapon("Лук Дракона"));

            for (var i = 0; i < 1000; i++) BadsWeapons.Add(new Weapon("Лук Темных Эльфов"));

            GoodWeaponsView.ItemsSource = GoodsWeapons;
            BadWeaponsView.ItemsSource = BadsWeapons;
            GoodWeaponsCountView.DataContext = GoodsWeapons;
            BadWeaponsCountView.DataContext = BadsWeapons;
            LoseStrikeOptionView.DataContext = this;
            MaxLoseStreak.DataContext = this;
            LogView.DataContext = this;
        }


        public bool DoEnchant(Weapon weapon)
        {
            Thread.Sleep((int)JilRandomizer.Count(300, 500));
            if (JilRandomizer.Chance(70))
            {
                weapon.Enchant++;
                return true;
            }
            return false;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ThreadPool.QueueUserWorkItem(x => GetLoseStreak());
        }

        private void GetLoseStreak()
        {
            while(BadsWeapons.Count != 0)
            {
                var currentWeapon = BadsWeapons.FirstOrDefault(a => a.Enchant == BadsWeapons.Min(b => b.Enchant));
                if (currentWeapon == null)
                {
                    MessageBox.Show("Кончались пухи");
                    return;
                }
                if (DoEnchant(currentWeapon)) CurrentMaxLooseStreak = 0;
                else
                {
                    Dispatcher?.Invoke(() => BadsWeapons.Remove(currentWeapon));
                    CurrentMaxLooseStreak++;
                }
                if (LooseStrikeCount != CurrentMaxLooseStreak) continue;
                Log = $"Поймали луз стрик из {LooseStrikeCount}";
                TryToEnchantAfterLooseStreak();
                return;
            }
            MessageBox.Show("Кончались пухи");
            return;
        }

        private void TryToEnchantAfterLooseStreak()
        {
            while (true)
            {
                var currentWeapon = GoodsWeapons.FirstOrDefault(a => a.Enchant == GoodsWeapons.Min(b => b.Enchant));
                if (currentWeapon == null)
                {
                    MessageBox.Show("Кончались пухи");
                    return;
                }
                if (DoEnchant(currentWeapon))
                {
                    CurrentMaxLooseStreak = 0;
                    MessageBox.Show($"Успешно точнули ДОРОГУЮ пуху {currentWeapon.WeaponName} на +{currentWeapon.Enchant}");
                    return;
                }
                MessageBox.Show($"Сломали ДОРОГУЮ пуху {currentWeapon.WeaponName} на +{currentWeapon.Enchant}");
                //Точим до победного
                Dispatcher?.Invoke(() => GoodsWeapons.Remove(currentWeapon));
                CurrentMaxLooseStreak++;
            }
        }
    }
}
