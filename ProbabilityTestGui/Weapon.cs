﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using ProbabilityTestGui.Annotations;

namespace ProbabilityTestGui
{
    public class Weapon : INotifyPropertyChanged
    {
        private string _weaponName;
        private int _enchant = 3;

        public Weapon(string name)
        {
            WeaponName = name;
        }

        public string WeaponName
        {
            get => _weaponName;
            set
            {
                _weaponName = value;
                OnPropertyChanged();
            }
        }

        public int Enchant
        {
            get => _enchant;
            set
            {
                _enchant = value;
                OnPropertyChanged();
            } 
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
