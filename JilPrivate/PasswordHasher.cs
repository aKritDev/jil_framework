﻿using System;
using System.Text;
using JilPrivate.Utils;
using Org.BouncyCastle.Crypto.Digests;

namespace JilPrivate
{
    public ref struct PasswordHasher
    {
        private readonly Span<byte> _staticSalt;

        public PasswordHasher(object key) =>
            _staticSalt = //max size of password = 32 symbols in unicode(if 1 symbol = 4 dynamicSalt)
            "5789bd5257398ba934f5294e9491b68a6f00c3087ca08e919ef602393fa992e96668bdbb872d9f8ab962e4d83ab0c8c11ce9db784b7965f91073daa1d5ce8329e644806420ccdf8db183e531d221527d1814b460c837a602fe71caf4ede20ab25d0c65be269cc4bfb29c9ad80f0e200c47a207357348a7d5a239afefba0c4003".StringToBytes();
        /// <summary>
        /// Pollutes the password with static and dynamic salt
        /// </summary>
        /// <param name="password"></param>
        /// <param name="dynamicSalt"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">passwordBytes.Length can't be more then 128 bytes and dynamicSalt.Length must be 128 bytes</exception>
        public unsafe Span<byte> MakeHashing(string password, ref Span<byte> dynamicSalt)
        {
            var passwordBytes = Encoding.Unicode.GetBytes(password);
            var passBytesCount = passwordBytes.Length;
            if (passBytesCount > 128) throw new ArgumentException("passwordBytes.Length can't be more then 128 bytes");
            if (dynamicSalt.IsEmpty)
            {
                dynamicSalt = new byte[128];
                Randomizer.RandomizeBytes(dynamicSalt);
            }
            Span<byte> bedragglePasswordBytes = stackalloc byte[passBytesCount * 3];
            var stackOffset = 0;
            for (var i = 0; i < passBytesCount; i++)
            {
                bedragglePasswordBytes[stackOffset++] = dynamicSalt[i];
                bedragglePasswordBytes[stackOffset++] = _staticSalt[i];
                bedragglePasswordBytes[stackOffset++] = passwordBytes[i];
            }
            var bedragglePass = bedragglePasswordBytes.ToArray();
            var crypt = new SkeinDigest(1024, 6144);
            crypt.BlockUpdate(bedragglePass, 0, bedragglePass.Length);
            var result = new byte[768];
            crypt.DoFinal(result, 0);
            return result;
        }
    }
}
