﻿using System;
using System.Security.Cryptography;

namespace JilPrivate
{
    public static class Randomizer
    {
        /// <summary>
        /// Заполняет массив случайными числами.
        /// </summary>
        /// <param name="bytes"></param>
        public static Span<byte> RandomizeBytes(Span<byte> bytes)
        {
            RandomInstance.GetNonZeroBytes(bytes);
            return bytes;
        }

        /// <summary>
        /// Заполняет массив случайными числами.
        /// </summary>
        /// <param name="size"></param>
        public static Span<byte> RandomizeBytes(int size)
        {
            var bytes = new byte[size];
            RandomInstance.GetNonZeroBytes(bytes);
            return bytes;
        }

        /// <summary>
        /// Возвращает true, если повезло, и указанный шанс сработал, иначе - false.
        /// Шанс указывается от 0 - 100. Может быть дробным числом. Пример chance = 0.0001
        /// </summary>
        /// <param name="chance"></param>
        /// <returns></returns>
        public static bool Chance(double chance) => GetRndDouble() * 100D <= chance;

        /// <summary>
        /// Возвращает случайное значение в диопазоне от min, до max типа long
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static long Count(long min, long max) => (long)Math.Floor(min + ((double)max - min) * GetRndDouble());

        /// <summary>
        /// Возвращает случайное значение в диопазоне от min, до max типа long
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static long Count(float min, float max) => (long)Math.Floor(min + ((double)max - min) * GetRndDouble());

        /// <summary>
        /// Возвращает случайное значение в диопазоне от min, до max типа long
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static long Count(ulong min, ulong max) => (long)Math.Floor(min + ((double)max - min) * GetRndDouble());


        /// <summary>
        /// Возвращает случайное число типа double
        /// </summary>
        /// <returns></returns>
        public static double GetRndDouble()
        {
            var data = new byte[4];
            RandomInstance.GetNonZeroBytes(data);
            return BitConverter.ToUInt32(data, 0) / (uint.MaxValue + 1.0);
        }

        /// <summary>
        /// Возвращает случайное число типа int
        /// </summary>
        /// <returns></returns>
        public static int GetRndInt()
        {
            var data = new byte[4];
            RandomInstance.GetNonZeroBytes(data);
            return BitConverter.ToInt32(data, 0);
        }

        private static readonly RNGCryptoServiceProvider RandomInstance = new RNGCryptoServiceProvider();
    }
}
