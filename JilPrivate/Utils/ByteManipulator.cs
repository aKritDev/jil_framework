﻿using System;
using System.Text;

namespace JilPrivate.Utils
{
    public static class ByteManipulator
    {
        public static void WriteMerged(this Span<byte> buffer, ref int offset, Span<byte> first, Span<byte> second, ref int checksum)
        {
            var fullSize = first.Length * 2;
            var slice = buffer.Slice(offset, fullSize);
            var localOffset = 0;
            for (var i = 0; i < first.Length; i++)
            {
                slice[localOffset] = first[i];
                slice[localOffset + 1] = second[i];
                checksum += first[i];
                checksum += second[i];
                localOffset += 2;
            }
            offset += fullSize;
        }

        private static unsafe Span<byte> ReadFromMerged(this Span<byte> buffer, int size, ref int offset, ref int checksum, bool readFirst)
        {
            var sliced = buffer.Slice(offset, size * 2);
            Span<byte> resultBytes = stackalloc byte[size];
            var localOffset = 0;
            for (var i = 0; i < size; i++)
            {
                var first = sliced[localOffset];
                var second = sliced[localOffset + 1];
                resultBytes[i] = readFirst ? first : second;
                checksum += first + second;
                localOffset += 2;
            }

            offset += size * 2;
            return resultBytes.ToArray();
        }

        public static unsafe void WriteShort(this Span<byte> buffer, short @short, ref int offset, ref int checksum)
        {
            fixed (byte* ptr = buffer.Slice(offset, sizeof(short))) *(short*)ptr = @short;
            checksum += @short;
            offset += sizeof(short);
        }

        public static unsafe short ReadShort(this Span<byte> buffer, ref int offset, ref int checksum)
        {
            short result;
            fixed (byte* ptr = buffer.Slice(offset, sizeof(short))) result = *(short*)ptr;
            checksum += result;
            offset += sizeof(short);
            return result;
        }

        public static unsafe void WriteChecksum(this Span<byte> buffer, ref int offset, int checksum)
        {
            fixed (byte* ptr = buffer.Slice(offset, sizeof(int))) *(int*)ptr = checksum;
            offset += sizeof(int);
        }

        public static unsafe int ReadChecksum(this Span<byte> buffer, ref int offset)
        {
            fixed (byte* ptr = buffer.Slice(offset, sizeof(int))) return *(int*)ptr;
        }

        public static void WriteEnd(this Span<byte> buffer, ref int offset, Span<byte> bytes)
        {
            var slice = buffer.Slice(offset, bytes.Length);
            for (var i = 0; i < bytes.Length; i++) slice[i] = bytes[i];
        }

        public static unsafe DateTimeOffset ReadDateTimeFromMerged(this Span<byte> buffer, ref int offset, ref int checksum, bool isFirst)
        {
            fixed (byte* ptr = ReadFromMerged(buffer, sizeof(long), ref offset, ref checksum, isFirst)) return DateTimeOffset.FromFileTime(*(long*) ptr);
        }

        public static unsafe Guid ReadGuidFromMerged(this Span<byte> buffer, ref int offset, ref int checksum, bool isFirst)
        {
            fixed (byte* ptr = ReadFromMerged(buffer, sizeof(Guid), ref offset, ref checksum, isFirst)) return *(Guid*)ptr;
        }

        public static string ReadStringFromMerged(this Span<byte> buffer, short size, ref int offset, ref int checksum, bool readFirst)
            => Encoding.Unicode.GetString(ReadFromMerged(buffer, size, ref offset, ref checksum, readFirst));

        public static unsafe string BytesToString(this Span<byte> bytes)
        {
            Span<char> c = stackalloc char[bytes.Length * 2];
            for (int bx = 0, cx = 0; bx < bytes.Length; ++bx, ++cx)
            {
                var b = (byte)(bytes[bx] >> 4);
                c[cx] = (char)(b > 9 ? b + 0x37 + 0x20 : b + 0x30);
                b = (byte)(bytes[bx] & 0x0F);
                c[++cx] = (char)(b > 9 ? b + 0x37 + 0x20 : b + 0x30);
            }
            return new string(c);
        }

        public static Span<byte> StringToBytes(this string str)
        {
            if (str.Length == 0 || str.Length % 2 != 0) return new byte[0];
            Span<byte> buffer = stackalloc byte[str.Length / 2];
            for (int bx = 0, sx = 0; bx < buffer.Length; ++bx, ++sx)
            {
                var c = str[sx];
                buffer[bx] = (byte)((c > '9' ? c > 'Z' ? c - 'a' + 10 : c - 'A' + 10 : c - '0') << 4);
                c = str[++sx];
                buffer[bx] |= (byte)(c > '9' ? c > 'Z' ? c - 'a' + 10 : c - 'A' + 10 : c - '0');
            }
            return buffer.ToArray();
        }

        public static bool EqualsTo(this Span<byte> left, Span<byte> right)
        {
            if (left.Length != right.Length) return false;
            for (var i = 0; i < left.Length; i++) if (left[i] != right[i]) return false;
            return true;
        }
    }
}
