﻿using System;
using JilPrivate.Utils;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Parameters;

namespace JilPrivate
{
    internal static class Crypters
    {
        public enum CrypterName
        {
            CookieCrypterKey
        }

        private static readonly string[] BlockCrypterKeys =
        {
            "a9e335c434ca7b84977b73bab471f987404da6fdff6f21ba31d5dfef3e9bbd0c90f21bf97fca716b36a5efcca774068108d8239e3769" //CookieCrypterKey
        };



        public static Span<byte> Crypt(this Span<byte> bytes, CrypterName crypterName) => ProcessBytes(bytes, crypterName, true);

        public static Span<byte> Decrypt(this Span<byte> bytes, CrypterName crypterName) => ProcessBytes(bytes, crypterName, false);


        private static Span<byte> ProcessBytes(this Span<byte> buffer, CrypterName crypterName, bool forEncryption)
        {
            var crypter = new BufferedBlockCipher(new BlowfishEngine());
            crypter.Init(forEncryption, new KeyParameter(BlockCrypterKeys[(int)crypterName].StringToBytes().ToArray()));
            return crypter.ProcessBytes(buffer.ToArray(), 0, buffer.Length);
        }
    }
}
