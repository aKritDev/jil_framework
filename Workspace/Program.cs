﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using JilCore.Core;
using JilCore.Core.FastCore.Binary;
using JilCore.Core.Utils.Extensions;
using JilCore.Core.Utils.Log;
using JilPrivate;
using JilToolkit.Utils.Metric;
using Kernel.ByteRepresentation;

namespace Workspace
{

    internal class Program
    {
        private static readonly JilLog Log = JilLog.GetLogger(typeof(Program));
        private static readonly unsafe int DateTimeSize = sizeof(Guid);
        private static unsafe void Main()
        {
            var a = new List<Guid>
            {
                Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(),
                Guid.Parse("96708AFF-FD9D-4A9E-96E0-A3C95FC63B3B"), Guid.Parse("96708AFF-FD9D-4A9E-96E0-A3C95FC63B3A")
            };
            var b = new List<Guid> {
                Guid.NewGuid(), Guid.NewGuid(), Guid.Parse("96708AFF-FD9D-4A9E-96E0-A3C95FC63B3B"), Guid.NewGuid(),  Guid.Parse("96708AFF-FD9D-4A9E-96E0-A3C95FC63B3A"), Guid.NewGuid()};
            var result = a.Where(a => b.Any(b => b == a)).ToList();
            //TestSpeedChecks();
        }

        private static void TestSpeedChecks()
        {
            var tested = new Action(() =>
            {
                var temp = new List<string>();
                for (var i = 0; i < 1; i++) temp.Add($"Очень очень очень очень большая строка {i.ToString()} ");
                //foreach (var str in temp) Console.WriteLine(str);
            });
            long executionTime = 0, usedMemory = 0;
            CodeSpeedChecker.RunOnce(tested, ref executionTime, ref usedMemory);
            usedMemory = executionTime = 0;
            CodeSpeedChecker.RunOnce(tested, ref executionTime, ref usedMemory);
            usedMemory = executionTime = 0;
            Console.WriteLine("Прогрев окончен");
            CodeSpeedChecker.RunParallel(tested, 10_000, ref executionTime, ref usedMemory);
            CodeSpeedChecker.RunParallel(tested, 10_000, ref executionTime, ref usedMemory);
            CodeSpeedChecker.RunParallel(tested, 10_000, ref executionTime, ref usedMemory);
            CodeSpeedChecker.RunParallel(tested, 10_000, ref executionTime, ref usedMemory);
            CodeSpeedChecker.RunParallel(tested, 10_000, ref executionTime, ref usedMemory);
            Console.ReadLine();
        }

        private static void TestUdp()
        {
            var s = new UDPSocket();
            //s.Server("0.0.0.0", 2108);

            UDPSocket c = new UDPSocket();
            c.Send("TEST!", "127.0.0.1", 2108);

            Console.ReadKey();
        }

        private static void TestNewCookieGen()
        {
            var guid = Guid.NewGuid();
            var userAgentStart = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0";
            var ipAddressStart = "127.0.0.1";
            var sw = new Stopwatch();
            for (int i = 0; i < 100; i++)
            {
                sw.Restart();
                var cookie = CookieProcessor.GenerateCookie(guid, userAgentStart, ipAddressStart);
                sw.Stop();
                Console.WriteLine($"{StopwatchExtension.ElapsedNanoSeconds(sw)}; {cookie}");
            }

            Console.ReadLine();
            var crypt = CookieProcessor.GenerateCookie(guid, userAgentStart, ipAddressStart);
            CookieProcessor.ParseCookie(crypt, out var createdTime, out var sessionId, out var userAgent, out var ipAddress);
            var res1 = guid == sessionId;
            var res2 = userAgentStart == userAgent;
            var res3 = ipAddressStart == ipAddress;
        }

        private static long TestHashing(Stopwatch sw, ref string resultString)
        {
            sw.Restart();
            var t = new PasswordHasher(null);
            var dynamicSalt = new Span<byte>();
            var result = t.MakeHashing(resultString, ref dynamicSalt);
            resultString = Encoding.Unicode.GetString(result);
            sw.Stop();
            return StopwatchExtension.ElapsedNanoSeconds(sw);
        }

        private static void Test4()
        {
            using BinaryWriterDynamicSize binaryWriterDynamicSizeStruct = default;
            // ReSharper disable once PossiblyImpureMethodCallOnReadonlyVariable

            for (var j = 0L; j < 1000_000_000; j++) binaryWriterDynamicSizeStruct.Write(j);
        }

        public static void Test1()
        {
            var bytes = new byte[] { 0xff, 0xff, 0xff };
            var b = ToInt32(bytes, 0);
            var bb = -16777216;
            var s = BitConverter.GetBytes(bb);
            Console.ReadLine();
        }

        public static void Test2()
        {
            var sw = new Stopwatch();
            for (var i = 0; i < 1_000; i++)
            {
                using BinaryWriterDynamicSize binaryWriterDynamicSizeStruct = default;
                //using JilByteStreamWrapper binaryWriterDynamicSizeStruct = new JilByteStreamWrapper(100);
                sw.Restart();
                for (var j = 0; j < 1000_000; j++) binaryWriterDynamicSizeStruct.Write(j);
                sw.Stop();
                Log.Info($"{StopwatchExtension.ElapsedNanoSeconds(sw)}");
               // Console.ReadLine();
            }
        }

        public static void Test3()
        {
            string b;
            unsafe
            {
                ushort x = 1;
                b = *&x == 0 ? "big-endian" : "little-endian";
            }
            //Log.Info(b);
            Console.ReadLine();
        }


        public static unsafe int ToInt32(byte[] value, int startIndex)
        {
            fixed (byte* pbyte = &value[startIndex]) return *((int*)pbyte);
        }

        public static void TestSocks()
        {
            var sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IPv4);
        }

        public static void TestWriter()
        {
            for (var i = 0; i < 1_000_000; i++)
            {
                var t = new TestObject {TestInt = 546516, TestInt2 = 1}.ToBytes();
            }
        }
    }
}
